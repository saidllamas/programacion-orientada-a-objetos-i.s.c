package ejem_sobrecarga;

public class Animales {
	
	public void sobrecarga(Animal a){
		System.out.println("Metodo de parametro animal");
	}//sobrecarga
	
	public void sobrecarga(Caballo c){
		System.out.println("Metodo de parametro caballo");
	}//sobrecarga
	
}//class
