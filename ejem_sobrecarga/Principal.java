package ejem_sobrecarga;

public class Principal {

	public static void main(String[] args) {
		Animales as = new Animales();
		Animal objAnimal = new Animal();
		Caballo objCaballo = new Caballo();
		
		as.sobrecarga(objAnimal);
		as.sobrecarga(objCaballo);
	}//main

}//class
