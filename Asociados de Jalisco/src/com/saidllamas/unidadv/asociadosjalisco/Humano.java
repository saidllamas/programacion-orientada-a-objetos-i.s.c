package com.saidllamas.unidadv.asociadosjalisco;

public class Humano extends Elemento{
	
	private String direccion;
	private long telefono;
	
	public String getDireccion() {
		return direccion;
	}//getDireccion
	
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}//setDireccion
	
	public long getTelefono() {
		return telefono;
	}//getTelefono
	
	public void setTelefono(long telefono) {
		this.telefono = telefono;
	}//setTelefono
	
}//class
