package com.saidllamas.unidadv.asociadosjalisco;

public class Categoria extends Elemento{
	
	private String descripcion;

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
}//class
