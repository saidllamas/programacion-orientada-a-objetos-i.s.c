package com.saidllamas.unidadv.asociadosjalisco;

public class Ventas extends Elemento {

	private String fechaCompra;
	private int IDcompra;
	private int IDcliente;
	private double total;
	private int descuentoAplicado;
	
	public String getFechaCompra() {
		return fechaCompra;
	}
	public void setFechaCompra(String fechaCompra) {
		this.fechaCompra = fechaCompra;
	}
	public int getIDcompra() {
		return IDcompra;
	}
	public void setIDcompra(int iDcompra) {
		IDcompra = iDcompra;
	}
	public int getIDcliente() {
		return IDcliente;
	}
	public void setIDcliente(int iDcliente) {
		IDcliente = iDcliente;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public int getDescuentoAplicado() {
		return descuentoAplicado;
	}
	public void setDescuentoAplicado(int descuentoAplicado) {
		this.descuentoAplicado = descuentoAplicado;
	}
	
	
}
