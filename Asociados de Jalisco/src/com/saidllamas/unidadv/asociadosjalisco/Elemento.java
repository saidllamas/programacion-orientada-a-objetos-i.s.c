package com.saidllamas.unidadv.asociadosjalisco;

public class Elemento {
	
	private int ID;
	private String nombre;
	
	public int getID() {
		return ID;
	}//getID
	
	public void setID(int ID) {
		this.ID = ID;
	}//setID
	
	public String getNombre() {
		return nombre;
	}//getNombre
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}//setNombre
	
}//class
