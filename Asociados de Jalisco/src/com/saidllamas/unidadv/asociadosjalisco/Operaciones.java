package com.saidllamas.unidadv.asociadosjalisco;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.Vector;

public class Operaciones extends Comportamiento {
	int ID = 0, i = 0, idc = 0; // i: contador para clasificaciones, idc: identificador de compras
	InputStreamReader teclado = new InputStreamReader(System.in);
	BufferedReader entrada = new BufferedReader(teclado);
	Scanner ent = new Scanner(System.in); //me apoya para los datos que no se necesitan validar
	
	Vector<Proveedor> proveedores = new Vector<Proveedor>();
	Vector<Clientes> clientes = new Vector<Clientes>();
	Vector<Producto> productos = new Vector<Producto>();
	Vector<Categoria> categorias = new Vector<Categoria>();
	Vector<Ventas> ventas = new Vector<Ventas>();
	
	boolean error = false;
	
	public Operaciones(){
		while(true){
			menu();
		}
	}//constructor
	
	public void menu(){
		int opcion = 0;
		System.out.println("** Asociados de Jalisco **");
		System.out.println("Bienvenido, elija una opcion: ");
		System.out.println("1) Dar de alta un elemento");
		System.out.println("2) Dar de baja un elemento");
		System.out.println("3) Consultar un elemento");
		System.out.println("4) Registrar venta");
		System.out.println("0) Terminar");
		try {
			opcion =  Integer.parseInt(entrada.readLine());
			if(opcion == 0) System.exit(0);
			error = false;
		}catch (Exception a) {
			System.err.println("Error"+ a.getMessage());
			error = true;
		}finally{
			if(error) menu();
			else menu(opcion);
		}//finally
	}//menu
	
	public void menu(int opcion){
		int eleccion = 0;
		switch (opcion) {
		case 1:
			System.out.println("Elegiste dar de alta un elemento, ahora elije el tipo");
			System.out.println("1) Proveedor");
			System.out.println("2) Cliente");
			System.out.println("3) Producto");
			System.out.println("4) Categoria");
			try {
				eleccion =  Integer.parseInt(entrada.readLine());
				error = false;
			}catch (Exception a) {
				menu(opcion);
			}finally {
				if(!error) alta(eleccion);
			}//finally
			break;
		case 2: 
			System.out.println("Elegiste dar de baja un elemento, ahora elije el tipo");
			System.out.println("1) Proveedor");
			System.out.println("2) Cliente");
			System.out.println("3) Producto");
			System.out.println("4) Categoria");
			try {
				eleccion =  Integer.parseInt(entrada.readLine());
			}catch (Exception a) {
				menu(opcion);
			}finally {
				if(!error) baja(eleccion);
			}//finally
			break;
		case 3: 
			System.out.println("Elegiste consultar un elemento, ahora elije el tipo");
			System.out.println("1) Proveedor");
			System.out.println("2) Cliente");
			System.out.println("3) Producto");
			System.out.println("4) Categoria");
			System.out.println("5) Ventas");
			try {
				eleccion =  Integer.parseInt(entrada.readLine());
			}catch (Exception a) {
				menu(opcion);
			}finally {
				if(!error) consulta(eleccion);
			}//finally
			break;
		case 4:
			if(!proveedores.isEmpty() && !clientes.isEmpty() && !productos.isEmpty()){
				ventas.add(realizarCompra());
				idc++;
			}else System.out.println("No puedes registrar una venta por que no hay datos suficientes en proovedores, clientes y productos");
			break;
		default:
			menu(opcion);
			break;
		}//switch
	}//menu-sobrecargado

	public void alta(int eleccion){
		switch (eleccion) {
		case 1:
			System.out.println("Se va a agregar un proveedor");
			proveedores.add(datosProveedor());
			ID++;
			break;
		case 2:
			System.out.println("Se va a agregar un cliente");
			clientes.add(datosCliente());
			ID++;
			break;
		case 3:
			if(!proveedores.isEmpty() && !categorias.isEmpty()){
				System.out.println("Se va a agregar un producto");
				productos.add(datosProducto());
				ID++;	
			}else System.out.println("Imposible agregar producto sin tener provedores y/o categorias");
			break;
		case 4:
			System.out.println("Se va a agregar una clasificacion");
			categorias.add(datosClasificacion());
			break;
		default:
			break;
		}//switch
	}//alta
	
	public void baja(int eleccion){
		String name;
		switch (eleccion) {
		case 1:
			if(!proveedores.isEmpty()){
				System.out.println("Se va a eliminar un proveedor");
				System.out.println("Ingrese el nombre del proveedor");
				name = ent.nextLine();
				delete(1, name);
			}else System.out.println("No hay registro que eliminar");
			break;
		case 2:
			if(!clientes.isEmpty()){
				System.out.println("Se va a eliminar un cliente");
				System.out.println("Ingrese el nombre del cliente");
				name = ent.nextLine();
				delete(2, name);
			}else System.out.println("No hay registro que eliminar");
			break;
		case 3:
			if(!productos.isEmpty()){
				System.out.println("Se va a eliminar un producto");
				System.out.println("Ingrese el nombre del producto");
				name = ent.nextLine();
				delete(3, name);
			}else System.out.println("No hay registro que eliminar");
			break;
		case 4:
			if(!categorias.isEmpty()){
				System.out.println("Se va a eliminar una clasificacion");
				System.out.println("Ingrese el nombre del clasificacion");
				name = ent.nextLine();
				delete(4, name);
			}else System.out.println("No hay registro que eliminar");
			break;
		default:
			break;
		}//switch
	}//alta
	
	public void consulta(int eleccion){
		switch (eleccion) {
		case 1:
			System.out.println("Se va a consultar un proveedor");
			for(Proveedor p : proveedores){
				System.out.println("ID: "+p.getID());
				System.out.println("Nombre: "+p.getNombre());
				System.out.println("Direccion: "+p.getDireccion());
				System.out.println("Telefono: "+p.getTelefono());
				System.out.println("Pagina web: "+p.getPaginaWeb());
			}//for
			break;
		case 2:
			System.out.println("Se va a consultar un cliente");
			for(Clientes c : clientes){
				System.out.println("ID: "+c.getID());
				System.out.println("Nombre: "+c.getNombre());
				System.out.println("Direccion: "+c.getDireccion());
				int ca = c.getCantidad();
				for(int i = 0; i < ca; i++) 	System.out.println("Telefono: "+c.getTel(i));
			}
			break;
		case 3:
			System.out.println("Se va a consultar un producto");
			for(Producto p : productos){
				System.out.println("ID: "+p.getID());
				System.out.println("Nombre: "+p.getNombre());
				System.out.println("Precio: "+p.getPrecio());
				System.out.println("Stock: "+p.getStock());
				System.out.println("Proveedor: "+p.getNombreProveedor());
				System.out.println("Categoria: "+p.getCategoria());
			}
			break;
		case 4:
			System.out.println("Se va a consultar una clasificacion");
			for(Categoria c : categorias){
				System.out.println("ID: "+c.getID());
				System.out.println("Nombre: "+c.getNombre());
				System.out.println("Descripcion: "+c.getDescripcion());
			}
			break;
		case 5:
			if(!ventas.isEmpty() && !proveedores.isEmpty() && !clientes.isEmpty()){
				System.out.println("Se van a consutar todas las ventas");
				for(Ventas v : ventas){
					System.out.println("ID Compra "+v.getIDcompra());
					System.out.println("Fecha de compra: "+v.getFechaCompra());
					System.out.println("ID Cliente: "+v.getIDcliente());
					System.out.println("Total: "+v.getTotal());
					System.out.println("Descuento aplicado: "+v.getDescuentoAplicado());
				}//for
			}else System.out.println("No estan completos los registros. Imposible registrar venta");
			break;
		default:
			break;
		}//switch
	}//class
	
	public Proveedor datosProveedor(){
		int num = -1;
		long tel = -1;
		System.out.println("Solicitando datos para un proveedor");
		System.out.println("Ingrese el nombre");
		String nombre = ent.nextLine();
		System.out.println("Complete la direccion, ingrese la calle");
		String calle = ent.nextLine();
		//ent.nextLine();
		System.out.println("Ingrese el numero");
		do{
			try {
				num = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}
		}while(num == -1);
		ent.nextLine();
		System.out.println("Ingrese la colonia");
		String colonia = ent.nextLine();
		System.out.println("Ingrese la ciudad");
		String ciudad = ent.nextLine();
		String direccion = calle +" " + num+" "+colonia +" "+ciudad;
		System.out.println(direccion);
		System.out.println("Ingrese el numero de telefono");
		do{
			try {
				tel = Long.parseLong(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}
		}while(tel == -1);
		System.out.println("Ingrese pagina web");
		String pagWeb = ent.nextLine();
		Proveedor p = new Proveedor();
		p.setNombre(nombre);
		p.setDireccion(direccion);
		p.setPaginaWeb(pagWeb);
		p.setTelefono(tel);
		p.setID(ID);
		return p;
	}//datosProveedor
	
	public Clientes datosCliente(){
		int num = -1;
		long tel = -1;
		int can = -1;
		System.out.println("Solicitando datos para un cliente");
		System.out.println("Ingrese el nombre");
		String nombre = ent.nextLine();
		System.out.println("Complete la direccion, ingrese la calle");
		String calle = ent.nextLine();
		//ent.nextLine();
		System.out.println("Ingrese el numero");
		do{
			try {
				num = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}
		}while(num == -1);
		ent.nextLine();
		System.out.println("Ingrese la colonia");
		String colonia = ent.nextLine();
		System.out.println("Ingrese la ciudad");
		String ciudad = ent.nextLine();
		String direccion = calle +" " + num+" "+colonia +" "+ciudad;
		System.out.println(direccion);
		System.out.println("Cuantos numeros de telefono incluye");
		do{
			try {
				can = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}
		}while(can == -1);
		Clientes c = new Clientes(can);
		for(int i = 0; i <can; i++){
			System.out.println("Ingrese el numero de telefono");
			do{
				try {
					tel = Long.parseLong(entrada.readLine());
				} catch (Exception e) {
					System.out.println("Error, se solicita un numero, vuelvalo a intentar");
				}
			}while(tel == -1);
			c.setTelefono(i, tel);
		}
		c.setNombre(nombre);
		c.setDireccion(direccion);
		c.setID(ID);
		return c;
	}//datosCliente
	
	public Producto datosProducto(){
		double precio = -1;
		boolean coincidencia = false;
		int stock = -1;
		String nombreProveedor, categoria;
		System.out.println("Solicitando datos para un producto");
		System.out.println("Ingrese el nombre");
		String nombre = ent.nextLine();
		System.out.println("Ingrese el precio");
		do{
			try {
				precio = Double.parseDouble(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}
		}while(precio == -1);
		do{
			try {
				stock = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}
		}while(stock == -1);
		
		do{
			System.out.println("Ingrese el nombre del proveedor");
			nombreProveedor = ent.nextLine();
			for(Proveedor p : proveedores){
				if(p.getNombre().equals(nombreProveedor)) coincidencia = true;
			}
			if(!coincidencia) System.out.println("No se encontro el proveedor, intentelo de nuevo");
		}while(!coincidencia);
		
		System.out.println("Ingrese cantidad en almacen");
		do{
			try {
				stock = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}
		}while(stock == -1);
		
		do{
			System.out.println("Ingrese la categoria a la que pertence");
			categoria = ent.nextLine();
			for(Categoria c : categorias){
				if(c.getNombre().equals(categoria)) coincidencia = true;
			}
			if(!coincidencia) System.out.println("No se encontro la categoria, intentelo de nuevo");
		}while(!coincidencia);
		
		System.out.println(nombre+precio+stock+nombreProveedor+categoria);
		Producto p = new Producto();
		p.setNombre(nombre);
		p.setPrecio(precio);
		p.setStock(stock);
		p.setNombreProveedor(nombreProveedor);
		p.setCategoria(categoria);
		p.setID(ID);
		//productos.add(p);
		//ID++;
		return p;
	}//datosProducto
	
	public Categoria datosClasificacion(){
		System.out.println("Solicitando datos para una clasificacion");
		System.out.println("Ingrese el nombre");
		String nombre = ent.nextLine();
		System.out.println("Ingrese la descripcion");
		String descripcion = ent.nextLine();
		Categoria c = new Categoria();
		c.setNombre(nombre);
		c.setDescripcion(descripcion);
		c.setID(i);
		i++;
		return c;
	}//datosClasificacion
	
	public void delete(int opc, String nombre){
		int ID = -1, c = 0;
		switch (opc) {
		case 1:
			for(Proveedor p: proveedores){
				if(p.getNombre().equals(nombre)){
					System.out.println("ID: "+p.getID());
					c++;
				}//if
			}//for
			System.out.println("Igrese el ID a eliminar");
			do{
				try {
					ID = Integer.parseInt(entrada.readLine());
				} catch (Exception e) {
					System.out.println("Error, se solicita un numero, vuelvalo a intentar");
				}//try
			}while(ID == -1);
			for(int i = 0; i < c; i++){
				if(proveedores.elementAt(i).getID() == ID){
					System.out.println("Eliminado");
					proveedores.remove(i);
				}//if
			}//for
			break;
		case 2:
			for(Clientes cl: clientes){
				if(cl.getNombre().equals(nombre)){
					System.out.println("ID: "+cl.getID());
				}//if
			}//for
			System.out.println("Igrese el ID a eliminar");
			do{
				try {
					ID = Integer.parseInt(entrada.readLine());
				} catch (Exception e) {
					System.out.println("Error, se solicita un numero, vuelvalo a intentar");
				}//try
			}while(ID == -1);
			for(int i = 0; i < c; i++){
				if(proveedores.elementAt(i).getID() == ID){
					System.out.println("Eliminado");
					proveedores.remove(i);
				}//if
			}//for
			break;
		case 3:
			for(Producto p: productos){
				if(p.getNombre().equals(nombre)){
					System.out.println("ID: "+p.getID());
				}//if
			}//for
			System.out.println("Igrese el ID a eliminar");
			do{
				try {
					ID = Integer.parseInt(entrada.readLine());
				} catch (Exception e) {
					System.out.println("Error, se solicita un numero, vuelvalo a intentar");
				}//try
			}while(ID == -1);
			for(int i = 0; i < c; i++){
				if(proveedores.elementAt(i).getID() == ID){
					System.out.println("Eliminado");
					proveedores.remove(i);
				}//if
			}//for
			break;
		case 4:
			for(Categoria cl: categorias){
				if(cl.getNombre().equals(nombre)){
					System.out.println("ID: "+cl.getID());
				}//if
			}//for
			break;
		}//switch
	}//delete
	
	public Ventas realizarCompra(){
		int c = 0;
		String producto;
		double t = 0, cost = 0;
		int descOpc = -1, descuentoP = -1;
		boolean clienteEncontrado = false, productoEncontrado = false;
		int compradorID = -1, productoID = -1, dia = -1, canti = -1;
		do{
			System.out.println("Ingrese nombre del cliente");
			String nombre = ent.nextLine();
			for(Clientes cl: clientes){
				if(cl.getNombre().equals(nombre)){
					System.out.println("ID: "+cl.getID());
					clienteEncontrado = true;
				}//if
			}//for
			if(!clienteEncontrado) System.out.println("Cliente no encontrado, ingrese otro nombre.");
		}while(!clienteEncontrado);
		
		System.out.println("Igrese el ID del comprador");
		do{
			try {
				compradorID = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}//try
		}while(compradorID == -1);
		for(int i = 0; i < c; i++){
			if(proveedores.elementAt(i).getID() == ID){
				compradorID = i;
			}//if
		}//for
		
		do{
			System.out.println("Igrese el nombre del producto a comprar");
			producto = ent.nextLine();
			for(Producto cl: productos){
				if(cl.getNombre().equals(producto)){
					System.out.println("ID: "+cl.getID()+", costo: "+cl.getPrecio());
					productoEncontrado = true;
				}//if
			}//for
			if(!productoEncontrado) System.out.println("Producto no encontrado, ingrese otro nombre.");
		}while(!productoEncontrado);
		
		System.out.println("Ingrese el ID del producto");
		do{
			try {
				productoID = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}//try
		}while(productoID == -1);
		for(Producto cl: productos){
			if(cl.getNombre().equals(producto) && cl.getID()==productoID){
				cost = cl.getPrecio();
			}//if
		}//for
		
		System.out.println("Ingrese la cantidad");
		do{
			try {
				canti = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}//try
		}while(canti == -1);
		
		System.out.println("¿Hay descuento?");
		System.out.println("1) Si. 2) No.");
		do{
			try {
				descOpc = Integer.parseInt(entrada.readLine());
			} catch (Exception e) {
				System.out.println("Error, se solicita un numero, vuelvalo a intentar");
			}//try
		}while(descOpc == -1);
		if(descOpc == 1){
			System.out.println("Ingrese el % del descuento en entero");
			do{
				try {
					descuentoP = Integer.parseInt(entrada.readLine());
				} catch (Exception e) {
					System.out.println("Error, se solicita un numero, vuelvalo a intentar");
				}//try
			}while(descuentoP == -1);
			double total =(canti * cost);
			t = total;
			total *= descuentoP;
			total /= 100;
			t -= total;
			System.out.println("Total con descuento: "+t);
		}else{
			descuentoP = 0; //no hubo descuento
			double total =(canti * cost);
			t = total;
			System.out.println("Total sin descuento: "+t);
		}
		
		System.out.println("Ingrese la fecha de compra, ingrese primero el dia");
		do{
			do{
				try {
					dia = Integer.parseInt(entrada.readLine());
				} catch (Exception e) {
					System.out.println("Error, se solicita un numero, vuelvalo a intentar");
				}//try
			}while(dia == -1);	
		}while(dia < 1 || dia > 31);
		
		System.out.println("Ingres el mes");
		String mes = ent.nextLine();
		
		String fecha = dia +", "+mes;
		Ventas v = new Ventas();
		v.setFechaCompra(fecha);
		v.setIDcliente(compradorID);
		v.setIDcompra(idc);
		v.setDescuentoAplicado(descuentoP);
		v.setTotal(t);
		return v;
	}//realizarCompra
	
}//class
