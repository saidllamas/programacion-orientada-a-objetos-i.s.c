package com.saidllamas.unidadv.asociadosjalisco;

public class Clientes extends Humano{
	
	private long telefonos[];
	private int cantidad;
	
	public Clientes(){	
	}//default
	
	public Clientes(int cantidad){
		telefonos = new long[cantidad];
		this.cantidad = cantidad;
	}//default
	
	public void setTelefono(int pos, long tel){
		telefonos[pos] = tel;
	}
	
	public long getTel(int pos){
		return telefonos[pos];
	}
	
	public long setTelefono(int pos){
		return telefonos[pos];
	}
	
	public int getCantidad(){
		return cantidad;
	}
}
