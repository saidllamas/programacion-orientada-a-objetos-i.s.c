package com.saidllamas.unidadv.asociadosjalisco;

public class Proveedor extends Humano{
	
	private String paginaWeb;

	public String getPaginaWeb() {
		return paginaWeb;
	}//getPaginaWeb

	public void setPaginaWeb(String paginaWeb) {
		this.paginaWeb = paginaWeb;
	}//setPaginaWeb
	
}//class
