package com.saidllamas.unidadv.asociadosjalisco;

public abstract class Comportamiento {
	
	public abstract void alta(int eleccion);
	public abstract void baja(int eleccion);
	public abstract void menu();
	public abstract void consulta(int eleccion);
	public abstract Proveedor datosProveedor();
	public abstract Clientes datosCliente();
	public abstract Producto datosProducto();
	public abstract Categoria datosClasificacion();
	public abstract void delete(int opc, String nombre);
	public abstract Ventas realizarCompra();
}
