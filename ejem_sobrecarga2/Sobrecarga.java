package ejem_sobrecarga2;

public class Sobrecarga{
	
	public void numeros(int x, int y){
		System.out.println("Metodo que recibe enteros");
	}//numeros
	
	public void numeros(double x, double y){
		System.out.println("Metodo que recibe flotantes");
	}//numeros
	
	public void numeros(String cadena){
		System.out.println("Metodo que recibe una cadena..."+cadena);
	}//numeros
	
}//class