package ejem_sobrecarga2;

public class Principal {

	public static void main(String[] args) {
		Sobrecarga s = new Sobrecarga();
		double a = 1.2, b = 1.4; 
		s.numeros("Hola");
		s.numeros(a, b);
		s.numeros(3, 4);
	}

}
