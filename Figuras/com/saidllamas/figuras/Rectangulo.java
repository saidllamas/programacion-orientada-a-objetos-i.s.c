package com.saidllamas.figuras;

import java.util.Scanner;

public class Rectangulo extends Figura{
	
	private Scanner entrada = new Scanner(System.in);
	private Figura rectangulo = new Figura();
	
	public Rectangulo(){
		System.out.println("Calculando el area de un Rectangulo");
		System.out.println("Ingrese la altura del rectangulo");
		int altura = entrada.nextInt();
		System.out.println("Ingrese la base del rectangulo");
		int base = entrada.nextInt();
		rectangulo.setAltura(altura);
		rectangulo.setBase(base);
	}
	
	public double calcularArea(){
		double area = rectangulo.getBase() * rectangulo.getAltura();
		return area;
	}//calcularArea
}
