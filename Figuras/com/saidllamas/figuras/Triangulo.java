package com.saidllamas.figuras;

import java.util.Scanner;

public class Triangulo extends Figura{
	
	private Scanner entrada = new Scanner(System.in);
	private Figura triangulo = new Figura();
	
	public Triangulo(){
		System.out.println("Calculando el area de un Triangulo");
		System.out.println("Ingrese la altura del triangulo");
		int altura = entrada.nextInt();
		System.out.println("Ingrese la base del triangulo");
		int base = entrada.nextInt();
		triangulo.setAltura(altura);
		triangulo.setBase(base);
	}//constructor

	
	public double calcularArea(){
		double area = (triangulo.getBase() * triangulo.getAltura()) / 2;
		return area;
	}//calcularArea
	
}//class
