package com.saidllamas.figuras;

import java.util.Scanner;

public class Operaciones {
	private Scanner entrada = new Scanner(System.in);
	public Operaciones(){
		int opc = 0;
		do{
			System.out.println("** Calculando area de figuras **");
			System.out.println("1) Calcular el area de un triangulo");
			System.out.println("2) Calcular el area de un cuadrado");
			System.out.println("3) Calcular el area de un circulo");
			System.out.println("4) Calcular el area de un rectangulo");
			System.out.println("    [0]Terminar.");
			opc = entrada.nextInt();
			switch (opc) {
			case 1:
				Figura triangulo = new Triangulo();
				System.out.println("El area de su triangulo es: "+triangulo.calcularArea());
				break;
			case 2:
				Figura cuadrado = new Cuadrado();
				System.out.println("El area de su cuadrado es: "+cuadrado.calcularArea());
				break;
			case 3:
				Figura circulo = new Circulo();
				System.out.println("El area de su circulo es: "+circulo.calcularArea());
				break;
			case 4:
				Figura rectangulo = new Rectangulo();
				System.out.println("El area de su rectangulo es: "+rectangulo.calcularArea());
				break;
			case 0:
				System.out.println("Fin del programa");
				break;
			default:
				System.out.println("Error en la seleccion de la figura");
				break;
			}
		}while(opc != 0);
	}//constructor
}//class