package com.saidllamas.figuras;

public class Figura {
	
	private int base;
	private int altura;
	
	protected void setBase(int base){
		this.base = base;
	}//setBase
	
	protected int getBase(){
		return base;
	}//getBase
	
	protected void setAltura(int altura){
		this.altura = altura;
	}//setAltura
	
	protected int getAltura(){
		return altura;
	}//getAltura
	
	public double calcularArea(){
		return 0;
	}//calcularArea
	
}//class
