package com.saidllamas.figuras;

import static java.lang.Math.*;
import java.util.Scanner;

public class Circulo extends Figura{
	
	private Scanner entrada = new Scanner(System.in);
	private int radio = 0;
	
	public Circulo(){
		System.out.println("Calculando el area de un Circulo");
		System.out.println("Ingrese el radio del circulo");
		radio = entrada.nextInt();
	}//constructor
	
	public double calcularArea(){
		double area = PI *(radio * radio);
		return area;
	}//calcularArea
	
}//class
