package com.saidllamas.figuras;

import java.util.Scanner;

public class Cuadrado extends Figura{
	
	private Scanner entrada = new Scanner(System.in);
	private Figura cuadrado = new Figura();
	
	public Cuadrado(){
		System.out.println("Calculando el area de un Cuadrado");
		System.out.println("Ingrese la altura del cuadrado");
		int base = entrada.nextInt();
		cuadrado.setBase(base);
	}//constructor
	
	public double calcularArea(){
		double area = (cuadrado.getBase() * cuadrado.getBase());
		return area;
	}//calcularArea
}
