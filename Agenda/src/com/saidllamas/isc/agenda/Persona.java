public class Persona {
	
	private String nombre = "", fechaNacimiento = "", direccion = "", correoElectronico = "";
	private int telefonoFijo = 0, telefonoMovil = 0;
	private int _id = 0;
	
				/*	METODOS SET	*/
	public Persona(){
		
	}//constructor default
	
	public Persona(String name, String date, String address, String email,	int telF, int telM, int id) {
		nombre = name;
		fechaNacimiento = date;
		direccion = address;
		correoElectronico = email;
		telefonoFijo = telF;
		telefonoMovil = telM;
		_id = id;
	}//constructor
				/*	METODOS GET	*/
	
	public String getNombre(){
		return nombre;
	}//getNombre
	
	public String getFechaNacimiento(){
		return fechaNacimiento;
	}//getFechaNacimiento
	
	public String getDireccion(){
		return direccion;
	}//getDireccion
	
	public String getCorreoElectronico(){
		return correoElectronico;
	}//getCorreoElectronico
	
	public int getTelefonoFijo(){
		return telefonoFijo;
	}//getTelefonoFijo
	
	public int getTelefonoMovil(){
		return telefonoMovil;
	}//getTelefonoMovil
	
	public int getId(){
		return _id;
	}//getId
	
}//class
