import java.util.Scanner;

public class OperacionesPersona {
	
	Scanner entrada; //entradas por teclado
	Persona objPersona = new Persona(); //Persona agregada
	Persona[] personas = new Persona[100]; //Personas agregadas 1-100
	private int _id = 0; //identificador unico para cada persona
	private int[] identificadores = new int[100]; //variable que me apoya para editar varios contactos con el mismo nombre pero diferentes personas
	private int IDmayor, IDultimo; //guarda el mayor ID registrado para no hacer consultas inecesarias y ultimo ID usado
	private boolean huboEliminacion = false;
	
	public OperacionesPersona(){
		int opc = 0; //controlar menu
		entrada = new Scanner(System.in);
		System.out.println("***	AGENDA	***");
		do{
			System.out.println("MENU: ");
			System.out.println("1) Agregar contacto. ");
			System.out.println("2) Editar contacto. ");
			System.out.println("3) Borrar contacto. ");
			System.out.println("4) Consultar contacto. ");
			System.out.println("	[0] Terminar. ");
			opc = entrada.nextInt();
			switch (opc) {
			case 1:
				agregarContacto();
				break;
			case 2: 
				editarContacto();
				break;
			case 3:
				borrarContacto();
				break;
			case 4: 
				consultarContacto();
				break;
			}
		}while(opc != 0);
	}//constructor vacio
	
	public void agregarContacto(){
		System.out.println("Ingrese nombre de la persona: ");
		String n = entrada.next();
		System.out.println("Ingrese fecha de nacimiento de la persona: ");
		String f = entrada.next();
		System.out.println("Ingrese domicilio de la persona: ");
		String d = entrada.next();
		System.out.println("Ingrese correo electronico de la persona: ");
		String e = entrada.next();
		System.out.println("Ingrese telefono fijo de la persona: ");
		int tf = entrada.nextInt();
		System.out.println("Ingrese telefono movil de la persona: ");
		int tm = entrada.nextInt();
		agregarContacto(n, f, d, e, tf, tm);
	}//agregarContacto
	
	public void agregarContacto(String name, String date, String address, String email, int telF, int telM){
		personas[_id] = new Persona(name, date, address, email, telF, telM, _id);
		System.out.println(personas[_id].getNombre()+" Agregado(a) correctamente");
		_id++; //una persona agregada
		if(_id>IDmayor){ IDmayor = _id;} //para consultar hasta aqui
		if(huboEliminacion){ _id = IDultimo; huboEliminacion = false;}
	}//agregarContacto
	
	public void editarContacto(){
		int identificadorModificar;
		System.out.println("Ingrese el nombre de la persona a modificar");
		String nombreModificar = entrada.next();
		int c = 0;
		for(int i = 0; i < IDmayor; i++){
			if(nombreModificar.equalsIgnoreCase(personas[i].getNombre())){
				identificadores[c] = i;
				c++;
			}//if
		}//for
		
		//for temporal
		System.out.println("Identificadores encontrados con el mismo nombre");
		for (int i = 0; i < 100; i++){
			if(identificadores[i]!=0) System.out.println(identificadores[i]);
		}
		System.out.println("Elija con el identificador a la persona que desea moficar los datos, [-1] cancela");
		identificadorModificar = entrada.nextInt();
      if(identificadorModificar > -1) editarContacto(identificadorModificar);
		//termina for temporal
		
		for(int i = 0; i < _id; i++){//limpiador de indentificadores
			identificadores[i]=0;
		}
	}//editarContacto
	
	public void editarContacto(int id){
		String n = personas[id].getNombre();//variables que no se modifican
		String f = personas[id].getFechaNacimiento();//variables que no se modifican
		System.out.println("Ingrese el nuevo domicilio de "+n);
		String d = entrada.next();
		System.out.println("Ingrese el nuevo correo electronico de "+n);
		String e = entrada.nextLine();
      e = entrada.next();//por error en java que no toma esta entradda
		System.out.println("Ingrese el nuevo numero de telefono fijo de "+n);
		int tf = entrada.nextInt();
		System.out.println("Ingrese el nuevo numero de telefono movil de "+n);
		int tm = entrada.nextInt();
		personas[id] = new Persona(n, f, d, e, tf, tm, id);
	}//editarContacto
	
	public void borrarContacto(){
		System.out.println("Ingrese el nombre de la persona a eliminar");
		String nombreModificar = entrada.next();
		int c = 0;
		for(int i= 0; i < _id; i++){
			if(nombreModificar.equalsIgnoreCase(personas[i].getNombre())){
				identificadores[c] = i;
				c++;
			}//if
		}//for
		
		//for temporal
		System.out.println("vienen los identificadores encontrados con el mismo nombre");
		for (int i = 0; i < 100; i++){
			if(identificadores[i]!=0) System.out.println(identificadores[i]);
		}
		System.out.println("Elija con el identificador a la persona que desea moficar los datos");
		int identificadorModificar = entrada.nextInt();
		borrarContacto(identificadorModificar);
		//termina for temporal
	}//borrarContacto
	
	public void borrarContacto(int id){
		String msj = personas[id].getNombre();
		IDultimo = _id; //respaldo el id ultimo id para despues agregar desde este punto
		personas[id] = new Persona("", "", "", "", 0, 0, id);//valores en blanco
		_id = id; //identificador libre entonces la proxima persona ocupara este ID
		System.out.println(msj+" eliminado de la lista de contactos");
		huboEliminacion = true;
	}//borrarContacto
	
	public void consultarContacto(){
      int opc = 0;
      do{
         System.out.println("1) Consultar por nombre");
         System.out.println("2) Consulta general");
         opc = entrada.nextInt();
      }while(opc < 1 || opc > 2);
      
      if (opc == 1){
         System.out.println("Ingrese nombre del contacto");
         String name = entrada.next();
         for(int i = 0; i < IDmayor; i++){
            if(personas[i].getNombre().equals(name)){
               System.out.print("Nombre: "+personas[i].getNombre()+", ");
      			System.out.print("Fecha de nacimiento: "+personas[i].getFechaNacimiento()+", ");
      			System.out.print("Direccion: "+personas[i].getDireccion()+", ");
      			System.out.print("Correo electronico: "+personas[i].getCorreoElectronico()+", ");
      			System.out.print("Telefono fijo: "+personas[i].getTelefonoFijo()+", ");
      			System.out.print("Telefono movil: "+personas[i].getTelefonoMovil()+".");
      			System.out.println("");
            }//if
         }//for
      }else if(opc == 2){
   		System.out.println("* Consulta de contactos *");
   		for (int i = 0; i < IDmayor; i++){
   			if(personas[i].getNombre().equals("")) continue; //esta vacio, se brinca al siguiente id
   			System.out.print("Identificador "+personas[i].getId()+":");
   			System.out.print("Nombre: "+personas[i].getNombre()+", ");
   			System.out.print("Fecha de nacimiento: "+personas[i].getFechaNacimiento()+", ");
   			System.out.print("Direccion: "+personas[i].getDireccion()+", ");
   			System.out.print("Correo electronico: "+personas[i].getCorreoElectronico()+", ");
   			System.out.print("Telefono fijo: "+personas[i].getTelefonoFijo()+", ");
   			System.out.print("Telefono movil: "+personas[i].getTelefonoMovil()+".");
   			System.out.println("");
   		}//for
      }//if
	}//borrarContacto
	
}//class
