package Ejem_Herencia;

import java.util.Vector;

public class Principal{

   public static void main(String[] args){
      
      Becario objBec = new Becario();
      objBec.setNombre("Juan Jose");
      objBec.setClases("Biologia, Quimica");
      objBec.setGrado(2);
      objBec.setGrupo('B');
      objBec.setPromedio(9.5);
      
      Regular objReg = new Regular();
      objReg.setNombre("Maria Jose");
      objReg.setClases("Biologia, Matematicas");
      objReg.setGrado(3);
      objReg.setGrupo('C');
      objReg.setPago(1750.0);
      
      System.out.println("*** Alumno Becario ***");
      System.out.println(objBec.getNombre());
      System.out.println(objBec.getClases());
      System.out.println(objBec.getGrado());
      System.out.println(objBec.getGrupo());
      System.out.println(objBec.getPromedio());
      System.out.println();
      
      System.out.println("*** Alumno  Regular ***");
      System.out.println(objReg.getNombre());
      System.out.println(objReg.getClases());
      System.out.println(objReg.getGrado());
      System.out.println(objReg.getGrupo());
      System.out.println(objReg.getPago());
      
      //Clase Vector
      Vector <Becario> vectBec = new Vector <Becario>();
      vectBec.add(objBec);
      //vectBec.add(objReg);
      
      Becario objBec1 = new Becario();
      for(int i = 0; i < vectBec.size(); i++){
         objBec1 = (Becario) vectBec.elementAt(i);
         System.out.println(objBec1.getNombre());
         System.out.println(objBec1.getClases());
         System.out.println(objBec1.getGrado());
         System.out.println(objBec1.getGrupo());
         System.out.println(objBec1.getPromedio());
      }//for
   }//main
}//class