package Ejem_Herencia;

public class Becario extends Alumno{

   private double promedio;
   
   public Becario(){
      promedio = 0.0;
   }//constructor
   
   public double getPromedio(){
      return promedio;
   }//getPromedio
   
   public void setPromedio(double promedio){
      this.promedio = promedio;
   }//promedio
}//class