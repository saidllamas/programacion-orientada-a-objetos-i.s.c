package Ejem_Herencia;

public class Regular extends Alumno{

   private double pago_insc;
   
   public Regular(){
      pago_insc = 0.0;
   }//constructor
   
   public double getPago(){
      return pago_insc;
   }//getPago
   
   public void setPago(double pago_insc){
      this.pago_insc = pago_insc;
   }
}//class