package Ejem_Herencia;

public class Alumno{

   protected String nombre, clases;
   protected int grado;
   protected char grupo;
   
   public Alumno(){
      nombre = "";
      clases = "";
      grado = 0;
      grupo = ' ';
   }//constructor
   
   //Get
   public String getNombre(){
      return nombre;
   }
   
   public String getClases(){
      return clases;
   }
   
   public int getGrado(){
      return grado;
   }
   
   public  char getGrupo(){
      return grupo;
   }
   
   //Set
   public void setNombre(String nombre){
      this.nombre = nombre;
   }
   
   public void setClases(String clases){
      this.clases = clases;
   }
   
   public void setGrado(int grado){
      this.grado = grado;
   }
   
   public void setGrupo(char grupo){
      this.grupo = grupo;
   }
}//class