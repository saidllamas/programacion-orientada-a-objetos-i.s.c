package com.saidllamas.empleados;

public class EmpleadoProyecto extends Empleado {
	
	private int horasTrabajadas;
	private int cuotaHora;
   private int dias;
	
	protected void setHorasTrabajadas(int horasTrabajadas){
		this.horasTrabajadas = horasTrabajadas;
	}//setHorasTrabajadas
	
	protected int getHorasTrabajadas(){
		return horasTrabajadas;
	}//getHorasTrabajadas
	
	protected void setCuotaHora(int cuotaHora){
		this.cuotaHora = cuotaHora;
	}//setCuotaHora
	
	protected int getCuotaHora(){
		return cuotaHora;
	}//getCuotaHora
   
   protected void setDias(int dias){
      this.dias = dias;   
   }//setDias
   
   protected int getDias(){
      return dias;
   }//getDias
}
