package com.saidllamas.empleados;

public class EmpleadoHoras extends Empleado {
	
	private int horasTrabajadas;
	private int cuotaHora;
	
	protected void setHorasTrabajadas(int horasTrabajadas){
		this.horasTrabajadas = horasTrabajadas;
	}//setHorasTrabajadas
	
	protected int getHorasTrabajadas(){
		return horasTrabajadas;
	}//getHorasTrabajadas
	
	protected void setCuotaHora(int cuotaHora){
		this.cuotaHora = cuotaHora;
	}//setCuotaHora
	
	protected int getCuotaHora(){
		return cuotaHora;
	}//getCuotaHora
}
