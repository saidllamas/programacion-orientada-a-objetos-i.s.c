package com.saidllamas.empleados;

public class EmpleadoAsalariado extends Empleado{
	
	private int salarioMensual;
	
	protected void setSalarioMensual(int salarioMensual){
		this.salarioMensual = salarioMensual;
	}//setSalarioMensual
	
	protected int getSalarioMensual(){
		return salarioMensual;
	}//getSalarioMenual

}
