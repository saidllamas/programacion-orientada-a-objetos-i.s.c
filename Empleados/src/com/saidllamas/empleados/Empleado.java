package com.saidllamas.empleados;

public class Empleado {
	
	private String nombreEmpleado;
	private String departamento;
	private String puesto;
	
	protected void setNombreEmpleado(String nombreEmpleado){
		this.nombreEmpleado = nombreEmpleado;
	}//setNombreEmpleado
	
	protected String getNombreEmpleado(){
		return nombreEmpleado;
	}//getNombreEmpleado
	
	protected void setDepartamento(String departamento){
		this.departamento = departamento;
	}//setDepartamento
	
	protected String getDepartamento(){
		return departamento;
	}//getDepartamento
	
	protected void setPuesto(String puesto){
		this.puesto = puesto;
	}//setPuesto
	
	protected String getPuesto(){
		return puesto;
	}//getPuesto
	
}//class
