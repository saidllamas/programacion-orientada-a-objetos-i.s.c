package com.saidllamas.empleados;

import java.util.Scanner;
import java.util.Vector;

public class Operaciones {
	
	private Vector <EmpleadoHoras> empleadosHora = new Vector<EmpleadoHoras>();
	private Vector <EmpleadoAsalariado> empleadosAsalariado = new Vector<EmpleadoAsalariado>();
	private Scanner entrada = new Scanner(System.in);
	private boolean alta = false, noEncontrado = false;
	private final double ISR = 0.08;
	
	public Operaciones(){
		int opc = 0;
		do{
			System.out.println("*** Guardianes de la Galaxia ***");
			System.out.println("MENU...");
			System.out.println("1) Alta empleado");
			System.out.println("2) Consulta empleado");
			System.out.println("3) Baja empleado");
			System.out.println("4) Modificar empleado");
			System.out.println("5) Calcular salario");
			System.out.println("6) Calcular nomina total");
			System.out.println("[0] Terminar");
			opc = entrada.nextInt();
			switch(opc){
				case 0: System.out.println("Fin del programa.");
					break;
				case 1:
					int tipoEmpleado = getTipoEmpleado();
					if(tipoEmpleado > 0){
						altaEmpleado(tipoEmpleado);
					}
					break;
				case 2:
					if(alta)consultarEmpleado();
					else System.out.println("Error, no hay registros a consultar");
					break;
				case 3:
					if(alta) bajaEmpleado();
					else System.out.println("Error, no hay registros a eliminar");
					break;
				case 4:
					if(alta) modificarEmpleado();
					else System.out.println("Error, no hay registros a modificar");
					break;
				case 5:
					if(alta) calcularSalario();
					else System.out.println("Error, imposible calcular sin registros");
					break;
				case 6:
					if(alta) calcularNominaTotal();
					else System.out.println("Error, imposible calcular sin registros");
					break;
				default: System.out.println("Error, regresando al men�");
			}//switch
		}while(opc != 0);
	}//constructor
	
	public int getTipoEmpleado(){
		int tipo = 0;
		do{
			System.out.println("Ingrese el tipo de empleado: ");
			System.out.println("1) Empleado por hora");
			System.out.println("2) Empleado asalariado");
			tipo = entrada.nextInt();
		}while(tipo > 2 || tipo < 1);
		return tipo;
	}
	
	public int getID(String name, int tipo){
		boolean coincide = false;
		String msj = "";
		int is = 0, idSeleccionado = 0;
		int[] id = new int[empleadosAsalariado.size()+empleadosHora.size()]; //me aseguro de que todos tengan un ID
		if(tipo == 1){
			for(int i = 0; i < empleadosHora.size(); i++){
				if(empleadosHora.elementAt(i).getNombreEmpleado().equalsIgnoreCase(name)){
					//System.out.println("Encontrado en la posicion: "+i);
					if(i == 0 && empleadosHora.elementAt(0).getNombreEmpleado().equalsIgnoreCase(name)) msj = "     "+name+" id: 0";
					id[is] = i;
					is++;
				}//if-consulta
			}//for
		}else if(tipo == 2){
			for(int i = 0; i < empleadosAsalariado.size(); i++){
				if(empleadosAsalariado.elementAt(i).getNombreEmpleado().equalsIgnoreCase(name)){
					//System.out.println("Encontrado en la posicion: "+i);
					if(i == 0 && empleadosAsalariado.elementAt(0).getNombreEmpleado().equalsIgnoreCase(name)) msj = "     "+name+" id: 0";
					id[is] = i;
					is++;
				}//if-consulta
			}//for
		}else{
			System.out.println("Ocurrio un error inesperado al encontrar el ID por el tipo de empleado");
		}
		
		if(msj.isEmpty()) noEncontrado = true;
		System.out.println("Elija el ID del empleado:");
		System.out.println(msj); // imprimo si se encontro en la posicion 0
		for(int i: id){
			if(i == -1) System.out.println("Sin IDs, marque cualquier numero para continuar");
			else if(i > 0) System.out.println("     "+name+" id:"+i);
		}//for
		int opc = entrada.nextInt();
		
		//COMPROBACION DE ID SELECCIONADO CON IDS MOSTRADOS
		for(int p = 0; p < id.length; p++){//comprobacion de id elegido
			if(id[p] == opc){ // cuando el ID elegido este entre los ids mostrados continuar con la eliminacion
				coincide = true;
				idSeleccionado = opc; 
			}//if comprobacion de id dentro de los id mostrados
		}//for
		if(coincide) return idSeleccionado;
		else return -1;
	}//getID
	
	public void altaEmpleado(int tipo){
		if(tipo == 1){
			int horasTrabajadas = 0, cuotaHora = 0;
			System.out.println("** Agregando Empleado por Hora **");
			System.out.println("Ingrese nombre del empleado:");
			String nombreEmpleado = entrada.next();
			System.out.println("Ingrese departamento de "+nombreEmpleado);
			String departamento = entrada.next();
			System.out.println("Ingrese el puesto de "+nombreEmpleado);
			String puesto = entrada.next();
			
			do{
				System.out.println("Ingrese horas trabajadas de "+nombreEmpleado);
				horasTrabajadas = entrada.nextInt();
			}while(horasTrabajadas <= 0);
			do{
				System.out.println("Ingrese cuota por hora "+nombreEmpleado);
				cuotaHora = entrada.nextInt();
			}while(cuotaHora <= 0);
			
			EmpleadoHoras empleado = new EmpleadoHoras();
			empleado.setNombreEmpleado(nombreEmpleado);
			empleado.setDepartamento(departamento);
			empleado.setPuesto(puesto);
			empleado.setHorasTrabajadas(horasTrabajadas);
			empleado.setCuotaHora(cuotaHora);
			empleadosHora.add(empleado);
			System.out.println(nombreEmpleado+" agreado correctamente");
			alta = true;
		}else if(tipo == 2){
			int salarioMensual = 0;
			System.out.println("** Agregando Empleado Asalariado **");
			System.out.println("Ingrese nombre del empleado:");
			String nombreEmpleado = entrada.next();
			System.out.println("Ingrese departamento de "+nombreEmpleado);
			String departamento = entrada.next();
			System.out.println("Ingrese el puesto de "+nombreEmpleado);
			String puesto = entrada.next();
			
			do{
				System.out.println("Ingrese salario mensual");
				salarioMensual = entrada.nextInt();
			}while(salarioMensual <= 0);
			
			EmpleadoAsalariado empleado = new EmpleadoAsalariado();
			empleado.setNombreEmpleado(nombreEmpleado);
			empleado.setDepartamento(departamento);
			empleado.setPuesto(puesto);
			empleado.setSalarioMensual(salarioMensual);
			empleadosAsalariado.add(empleado);
			System.out.println(nombreEmpleado+" agreado correctamente");
			alta = true;
		}else{
			System.out.println("Ocurrio un error inesperado en la alta por el tipo de empleado");
		}//else
	}//altaEmpleado
	
	public void consultarEmpleado(){
		int opc = 0;
		boolean encontrado = false;
		do{
			System.out.println("1) Mostrar todos los empleados");
			System.out.println("2) Consultar empleado");
			opc = entrada.nextInt();
		}while(opc < 1 || opc > 2);
		
		if(opc == 1){
			System.out.println("** Empleados asalariados ** ");
			for(EmpleadoAsalariado e : empleadosAsalariado){
				System.out.println("Empleado: "+e.getNombreEmpleado());
				System.out.println("En el departamento: "+e.getDepartamento());
				System.out.println("Con el puesto: "+e.getPuesto());
				System.out.println("Pago mensual: "+e.getSalarioMensual());
				System.out.println("*********************************************");
			}//for
			System.out.println("** Empleados por mes ** ");
			for(EmpleadoHoras e : empleadosHora){
				System.out.println("Empleado: "+e.getNombreEmpleado());
				System.out.println("En el departamento: "+e.getDepartamento());
				System.out.println("Con el puesto: "+e.getPuesto());
				System.out.println("Horas trabajadas: "+e.getHorasTrabajadas());
				System.out.println("Con un pago por hora de: "+e.getCuotaHora());
				System.out.println("*********************************************");
			}//for
		}else if(opc == 2){
			int tipo = getTipoEmpleado();
			System.out.println("Ingrese el nombre del empleado:");
			String consulta = entrada.next();
			if(tipo == 1){
				for(EmpleadoHoras e : empleadosHora){
					if(e.getNombreEmpleado().equalsIgnoreCase(consulta)){
						System.out.println(e.getNombreEmpleado()+" Encontrado como 'Empleado por Hora'");
						System.out.println("En el departamento: "+e.getDepartamento());
						System.out.println("Con el puesto: "+e.getPuesto());
						System.out.println("Horas trabajadas: "+e.getHorasTrabajadas());
						System.out.println("Con un pago por hora de: "+e.getCuotaHora());
						System.out.println("*********************************************");
						encontrado = true;
					}//if-consulta
				}//for
			}else if(tipo == 2){
				for(EmpleadoAsalariado e : empleadosAsalariado){
					if(e.getNombreEmpleado().equalsIgnoreCase(consulta)){
						System.out.println(e.getNombreEmpleado()+" Encontrado como 'Empleado Asalariado'");
						System.out.println("En el departamento: "+e.getDepartamento());
						System.out.println("Con el puesto: "+e.getPuesto());
						System.out.println("Pago mensual: "+e.getSalarioMensual());
						System.out.println("*********************************************");
						encontrado = true;
					}//if-consulta
				}//for
			}else{
				System.out.println("Ocurrio un error inesperado en la busqueda por el tipo de empleado");
			}//else--
		}else{
			System.out.println("Ocurrio un error inesperado en la busqueda");
		}//else
		if(encontrado != true && noEncontrado) System.out.println("No se encontraron registros.");
	}//consultarEmpleado
	
	public void bajaEmpleado(){
		int tipo = getTipoEmpleado();
		System.out.println("Ingrese el nombre del empleado:");
		String baja = entrada.next();
		if(tipo == 1){
			int ID = getID(baja, tipo);
			if(ID == -1) System.out.println("Ocurrio un error al elegir un ID");
			else{
				System.out.println(empleadosHora.elementAt(ID).getNombreEmpleado()+" eliminado correctamente");
				empleadosHora.remove(ID);
			}//else id
		}else if(tipo == 2){
			int ID = getID(baja, tipo);
			if(ID == -1) System.out.println("Ocurrio un error al elegir un ID");	
			else{
				System.out.println(empleadosAsalariado.elementAt(ID).getNombreEmpleado()+" eliminado correctamente");
				empleadosAsalariado.remove(ID);
			}//else id
		}else{
			System.out.println("Ocurrio un error inesperado en la baja");
		}//else
	}//bajaEmpleado
	
	public void modificarEmpleado(){
		int tipo = getTipoEmpleado();
		System.out.println("Ingrese el nombre del empleado a modificar");
		String modificar = entrada.next();
		int opc = getID(modificar, tipo);
		if(opc == -1){ System.out.println("Ocurrio un error al elegir un ID");
		}else{
			//�que desea modificar?
			if(tipo == 1){
				int m = 0;
				int horasTrabajadas = 0;
				int cuotaHora = 0;
				//�que desea modificar?
				do{
					System.out.println("�Que desea modificar?");
					System.out.println("1) Departamento");
					System.out.println("2) Puesto");
					System.out.println("3) Horas trabajadas");
					System.out.println("4) Cuota por hora");
					m = entrada.nextInt();
				}while(m < 1 || m > 4);
				//solicitar nuevos datos
				switch(m){
				case 1:
					System.out.println("Ingrese nuevo departamento de "+modificar);
					String departamento = entrada.next();
					empleadosHora.elementAt(opc).setDepartamento(departamento);
					break;
				case 2:
					System.out.println("Ingrese nuevo puesto de "+modificar);
					String puesto = entrada.next();
					empleadosHora.elementAt(opc).setPuesto(puesto);
					break;
				case 3:
					do{
						System.out.println("Ingrese nuevas horas trabajadas de "+modificar);
						horasTrabajadas = entrada.nextInt();
					}while(horasTrabajadas <= 0);
					empleadosHora.elementAt(opc).setHorasTrabajadas(horasTrabajadas);
					break;
				case 4: 
					do{
						System.out.println("Ingrese nueva cuota por hora "+modificar);
						cuotaHora = entrada.nextInt();
					}while(cuotaHora <= 0);
					empleadosHora.elementAt(opc).setCuotaHora(cuotaHora);
					break;
				}//switch
				System.out.println(modificar+" se ha modificado correctamente");
				//fin de datos
			}else if(tipo == 2){
				int m = 0;
				int salarioMensual = 0;
				//�que desea modificar?
				do{
					System.out.println("�Que desea modificar?");
					System.out.println("1) Departamento");
					System.out.println("2) Puesto");
					System.out.println("3) Cuota por mes");
					m = entrada.nextInt();
				}while(m < 1 || m > 4);
				switch(m){
				case 1:
					System.out.println("Ingrese nuevo departamento de "+modificar);
					String departamento = entrada.next();
					empleadosAsalariado.elementAt(opc).setDepartamento(departamento);
					break;
				case 2:
					System.out.println("Ingrese nuevo puesto de "+modificar);
					String puesto = entrada.next();
					empleadosAsalariado.elementAt(opc).setPuesto(puesto);
					break;
				case 3:
					do{
						System.out.println("Ingrese nuevo salario mensual");
						salarioMensual = entrada.nextInt();
					}while(salarioMensual <= 0);
					empleadosAsalariado.elementAt(opc).setSalarioMensual(salarioMensual);
					break;
				}//switch
				//fin de datos
				System.out.println(modificar+" se ha modificado correctamente");
			}//if-else
		}
	}//modificarEmpleado
	
	public void calcularSalario(){
		
		int tipo = getTipoEmpleado();
		System.out.println("Ingrese nombre del empleado a calcular");
		String salario = entrada.next();
		int ID = getID(salario, tipo);
		if(ID == -1)System.out.println("Ocurrio un error al elegir el ID ");
		else{
			if(tipo == 1){
				double total = empleadosHora.elementAt(ID).getCuotaHora() * empleadosHora.elementAt(ID).getHorasTrabajadas();
				double descuento = total * ISR;
				total -= descuento;
				System.out.println(salario+" gana: "+total);
			}else if(tipo == 2){
				double total = empleadosAsalariado.elementAt(ID).getSalarioMensual();
				double descuento = total * ISR;
				total -= descuento;
				System.out.println(salario+" gana: "+total);
			}//else tipo 2
			else{ System.out.println("Hubo un error inesperado por el tipo de dato");}
		}//else ID correcto
		
	}//calcularSalario
	
	public void calcularNominaTotal(){
		double hora = 0, asalariado = 0;
		double temp = 0, temp2;
		
		for(int i = 0; i < empleadosHora.size(); i++){
			 temp = empleadosHora.elementAt(i).getCuotaHora() * empleadosHora.elementAt(i).getHorasTrabajadas();
			 temp2 = temp * ISR;
			 hora += temp - temp2;
		}//for
		
		for(int i = 0; i < empleadosAsalariado.size(); i++){
			temp = empleadosAsalariado.elementAt(i).getSalarioMensual();
			temp2 = temp * ISR;
			asalariado += temp - temp2;
		}//for
		
		System.out.println("Suma de nominas: "+calcularNominaTotal(hora, asalariado));
	}//calcularSalario
	
	public double calcularNominaTotal(double sumaHoras, double sumaAsalariados){
		return sumaHoras + sumaAsalariados;
	}//calcularSalario
}//class
