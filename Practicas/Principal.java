import java.util.*;

public class Principal{

   public static void main(String[] args){
      Estudiante objEstudiante = new Estudiante();
      objEstudiante.pedirDatos();
      System.out.println("El promedio del alumno: "+objEstudiante.obtenerNombre()
                         +" es: "+objEstudiante.calcularPromedio());                  
   }//main
   
}//class