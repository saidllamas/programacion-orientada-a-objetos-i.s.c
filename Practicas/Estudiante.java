import java.util.*;

public class Estudiante{
   
   private String nombre;
   private int calif1, calif2, calif3, calif4;
   
   //¿cuando usar static?
   public void pedirDatos(){
      Scanner entrada = new Scanner(System.in);
      
      System.out.println("Ingrese nombre del alumno: ");
      nombre = entrada.nextLine();
      System.out.println("Ingrese calificacion 1: ");
      calif1 = entrada.nextInt();
      System.out.println("Ingrese calificacion 2: ");
      calif2 = entrada.nextInt();
      System.out.println("Ingrese calificacion 3: ");
      calif3 = entrada.nextInt();
      System.out.println("Ingrese calificacion 4: ");
      calif4 = entrada.nextInt();
      
   }//pedirDatos
   
   public double calcularPromedio(){
      return ((calif1+calif2+calif3+calif4)/4);
   }//calcularPromedio
   
   public String obtenerNombre(){
      return nombre;
   }
   
}//class