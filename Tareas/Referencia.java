import java.util.*;

public class Referencia{

   public static void main(String[] args){
      int x[] = new int[1];
      x[0] = 0;
      System.out.println("Valor de x: "+x[0]);
      cambiar(x);
      System.out.println("Valor de x: "+x[0]);
   }//main
   
   public static void cambiar(int y[]){
      y[0] = 1;
   }//cambiar
   
}//clase