import java.util.*;

/* Jesús Said Llamas Manriquez
*  Elaborar un programa que convierta un numero decimal a binario
*  octal y hexadecimal. Utilizando metodos y variables locales.
*  El codigo implementa los metodos: introducirNumero(), calcularBinario(),
*  calcularOctal() y calcularHexadecimal().
*  TAREA 15-02-16
*/

public class ConversionNumerosv2{

   public static void main(String[] args){
      introducirNumero();
   }//main
   
   public static void introducirNumero(){
      Scanner entrada = new Scanner(System.in);
      int numero;
      System.out.println("Ingrese numero");
      numero = entrada.nextInt();
      
      do{         
         if(numero == 1){//no hacer calculo e imprimir directamente "1"
            System.out.println("Binario: 1");
            System.out.println("Octal: 1");
            System.out.println("Hexadecimal: 1");
         }else{
            calcularBinario(numero); System.out.println();
            calcularOctal(numero); System.out.println();
            calcularHexadecimal(numero);
         }//if-else
         System.out.println("Ingrese nuevo numero ó [0] Terminar:");
         numero = entrada.nextInt();
      }while(numero != 0);   
   }//introducirNumero
   
   public static void calcularBinario(int numero){
   int entero = numero, modulo = 0, pos = 0;
      do{
         entero /= 2;
         modulo %= 2;
         modulo = entero;
         pos++;
      }while(entero != 1);
      int numeros[] = new int[pos];
      int limite = pos; entero = numero; modulo = numero; //reset valores
      pos = 0;
      System.out.print("Binario: 1 ");//1 que va siempre aparece como entero de la ultima division
      do{
         entero /= 2;
         modulo %= 2;
         numeros[pos] = modulo;
         modulo = entero;
         pos++;
      }while(entero != 1);
      for(int i = limite-1; i >= 0 ; i--){
         System.out.print(numeros[i]+" ");
      }//for
   }//calcularBinario
   
   public static void calcularOctal(int numero){
   int entero = numero, modulo = 0, pos = 0;
      do{
         entero /= 8;
         modulo %= 8;
         modulo = entero;
         pos++;
      }while(entero != 8 && entero > 0);
      int numeros[] = new int[pos];
      int limite = pos; entero = numero; modulo = numero; //reset valores
      pos = 0;
      System.out.print("Octal: ");
      do{
         entero /= 8;
         modulo %= 8;
         numeros[pos] = modulo;
         modulo = entero;
         pos++;
      }while(entero != 8 && entero > 0);
      for(int i = limite-1; i >= 0 ; i--){
         System.out.print(numeros[i]+" ");
      }//for
   }//calcularOctal
   
   public static void calcularHexadecimal(int numero){
   int entero = numero, modulo = 0, pos = 0;
      do{
         entero /= 16;
         modulo %= 16;
         modulo = entero;
         pos++;
      }while(entero != 16 && entero > 0);
      String numeros[] = new String[pos], m = "";
      int limite = pos; entero = numero; modulo = numero; //reset valores
      pos = 0;
      System.out.print("Hexadecimal: ");
      do{
         entero /= 16;
         modulo %= 16;
         m = String.valueOf(modulo);
         numeros[pos] = m;
         modulo = entero;
         pos++;
      }while(entero != 16 && entero > 0);
      for(int i = limite-1; i >= 0 ; i--){
         //System.out.print(numeros[i]+" ");
         int n = Integer.parseInt(numeros[i]);
         if(n > 9){
            switch(n){
               case 10: numeros[i] = "A";
               break;
               case 11: numeros[i] = "B";
               break;
               case 12: numeros[i] = "C";
               break;
               case 13: numeros[i] = "D";
               break;
               case 14: numeros[i] = "E";
               break;
               case 15: numeros[i] = "F";
               break;
           }//switch
         }//if
         System.out.print(numeros[i]+" ");
      }//for
         System.out.println();  //para salto de linear
   }//calcularHexadecimal
   
}//clase