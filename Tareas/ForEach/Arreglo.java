import java.util.*;

public class Arreglo{
   
   static int indice = 0;
   
   public void llenarArreglos(){
      Scanner entrada = new Scanner(System.in);
      int[] Arreglo1 = new int[10], Arreglo2 = new int[10];
      System.out.println("Llenar arreglo 1");
      for(int i = 0; i < Arreglo1.length; i++){
         Arreglo1[i] = entrada.nextInt();
      }//for
      
      System.out.println("Llenar arreglo 2");
      for(int i = 0; i < Arreglo2.length; i++){
         Arreglo2[i] = entrada.nextInt();
      }//for
      multiplicarArreglos(Arreglo1, Arreglo2);
   }//Llenar
   
   public void multiplicarArreglos(int[] Arreglo1, int[] Arreglo2){
      int[] resultado = new int[10];
      for(int array : Arreglo1){//arreglo1
         resultado[indice] = array;
         indice++;
      }//for
      indice = 0;
      //System.out.println("indice:"+indice);
      for(int i = Arreglo2.length-1; i >= 0; i--){//arreglo1
         resultado[indice] *= Arreglo2[i];
         System.out.println("" +resultado[indice]);
         indice++;
      }//for
   }//multiplicarArreglo
   
}//class