import java.util.*;

/*
*  Calcular el maximo comun divisor (mcd) de dos enteros mayores que cero
*/

public class MCD{
   
   public static void main(String[] args){
      capturarNumeros();
   }//main
   
   public static void capturarNumeros(){
      Scanner entrada = new Scanner(System.in);
      int n1, n2, mcd;
      do{
         System.out.println("Ingrese num1: ");
         n1 = entrada.nextInt();
      }while(n1 < 1);
      do{
         System.out.println("Ingrese num2: ");
         n2 = entrada.nextInt();
      }while(n2 < 1);   
     // calcularMCD(n1,n2);
      mcd = calcularMCD(n1,n2);
      System.out.println("El maximo comun divisor es: " +mcd);
   }//capturarNumeros
   
   public static int calcularMCD(int valor1, int valor2){
      int mcd = 0, may = 0;
      if(valor1 % valor2 == 0) mcd = valor2;
      else if(valor2 % valor1 == 0){ mcd = valor1;}
      else{
         for(int i = 1; i <= valor1 & i <= valor2; i++){
            //System.out.println(i);  //numeros generados
            if((valor1 % i == 0) && (valor2 % i == 0)) mcd = i;
         }//for
      }//else
      return mcd;
   }//calcularMCD
   
}//clase