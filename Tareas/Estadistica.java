import java.util.*;

/*
*  Jesus Said Llamas Manriquez I.S.C. 03/02/16
*  El siguiente codigo lee 15 numeros y calcula la media, varianza
*  y la desviacion, tiene los metodos:
*  leerNumeros(), calcularMedia(), calcularVarianza(), y calcularDesviancion()      
*  TAREA
*/

public class Estadistica {

   public static double _media = 0f, _varianza = 0f;

   public static void main(String[] args){
      leerNumeros();
   }//main
   
   public static void leerNumeros(){
      Scanner entrada = new Scanner(System.in);
      int numeros[] = new int[15];
      
      for(int i = 1; i <= 15; i++){
         System.out.println("Ingrese el numero: "+i);
         i--;//decremento para no desfasar al arreglo
         numeros[i] = entrada.nextInt();
         i++;//le incremento para seguir con el contador normalmente
      }//for
      System.out.println("La media es: "+calcularMedia(numeros));
      System.out.println("La varianza es: "+calcularVarianza(numeros));
      System.out.println("La desviacion es: "+calcularDesviacion());
   }//leerNumeros
   
   public static double calcularMedia(int[] numeros){
      int suma = 0;
      
      for(int i = 0; i < 15; i++){
         suma += numeros[i];
      }//for
      //System.out.println("Suma: "+suma);//imprimir suma de los numeros en el arreglo
      
      double media = suma / 15; //15 numeros ingresados
      _media = media;
      
      return media;      
   }//calcularMedia
   
   public static double calcularVarianza(int[] numeros){
      int elevacion = 2, valor = 0; //al cuadrado
      //Math.pow(,elevacion);//potencia
      for(int i = 0; i < 15; i++){
         valor += Math.pow(numeros[i],elevacion);
      }//for
      
      valor /= 15;
      valor -= Math.pow(_media,elevacion);
      _varianza = valor;
      
      return valor;
   }//calcularVarianza
   
   public static double calcularDesviacion(){
   
      double valor = Math.sqrt(_varianza);
      
      return valor;
   }//calcularDesviacion
   
}//clase