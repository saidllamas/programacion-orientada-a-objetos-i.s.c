import java.util.*;

/*
* Jesus Said Llamas Manriquez I.S.C. 29/01/16
* El siguiente codigo simula una calculadora, tiene tres metodos: mostrarMenu(), 
* entradaNumeros() y calcular(); realiza cuatro operaciones: suma, resta, multiplicacion
* y division.
*  TAREA   
*/

public class Calculadora{

   //variables
   private static int num_1, num_2, opc;
   private static Scanner entrada = new Scanner(System.in);
   
   //Metodos
   public static void main(String []args){
      mostrarMenu();//el programa arranca mostrando el menu de operaciones
   }//main
   
   public static void mostrarMenu(){      
      //instrucciones, mientras valores en opc sea menor que 0 o mayor que 4, solicitar operacion
      do{
         System.out.println("  M  E  N  U  ");
         System.out.println("1.- Sumar");
         System.out.println("2.- Restar");
         System.out.println("3.- Multipiclar");
         System.out.println("4.- Dividir");
         opc = entrada.nextInt();
      }while(opc < 1 || opc > 4);
      entradaNumeros();//ya tengo la operacion a realizar ahora a pedir valores
   }//menu
   
   public static void entradaNumeros(){
      //instrucciones, mientras valores en num_X sea 0 y la opcion sea dividir, solicitar valores
      do{
         System.out.println("Ingrese el primer numero");
         num_1 = entrada.nextInt();
      }while(num_1 == 0 && opc == 4);
      do{
         System.out.println("Ingrese el segundo numero");
         num_2 = entrada.nextInt();
      }while(num_2 == 0 && opc == 4);
      calcular(); //Ya tengo los valores cuidando el 0 en division ahora a calcular
   }//entradaNumeros
   
   public static void calcular(){
      //variables
      double resultado = 0f;
      //instrucciones
      switch(opc){
         case 1: resultado = num_1 + num_2;
         break;
         case 2: resultado = num_1 - num_2;
         break;
         case 3: resultado = num_1 * num_2;
         break;
         case 4: resultado = num_1 / num_2;
         break;
      }//switch
      System.out.println("Su resultado es: "+resultado);
      System.out.println("¿Desea un nuevo calculo? \n 1.- Si. Otro Numero.- No");
      opc = entrada.nextInt();
      if(opc == 1) mostrarMenu(); //1 elige hacer una operacion mas asi que muestro menu y se repite el proceso
      else { System.out.println("Calculadora terminada.");} // sino entonces termino el programa
   }//calcular
   
}