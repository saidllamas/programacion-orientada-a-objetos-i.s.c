import java.util.*;

public class EjemArreglo{

  private int Vec[] = new int [10], casilla = 0;
 
  public void Llenar(){
      Scanner entrada = new Scanner (System.in);
      
		for (int i=0; i<10; i++){
			System.out.print("Vec [" + (i+1)+ "]= ");
			Vec[i] = entrada.nextInt();
		}//for
      
	}//llenar
   
 public void Mostrar(){
	   System.out.println();
	   System.out.println(" *** ELEMENTOS DEL ARREGLO *** ");
	 		
		for (int i=0; i<10; i++){
			System.out.println("Vec [ " + (i+1) + " ]= " + Vec[i]);
		}//for
	}//Mostrar
   
   public boolean ParImpar(int cas){
     boolean par=false;
     Scanner entrada = new Scanner(System.in);
     cas -= 1;
     casilla = cas;
     if(Vec[cas]%2 == 0) par=true;
     
     return par;
  }//ParImpar
  
  public int ObtenerValor(){//metodo get
    return Vec[casilla];
  }//ObtenerValor

}//class