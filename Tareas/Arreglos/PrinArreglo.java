import java.util.*;

public class PrinArreglo
{
  public static void main (String []args)
  {
   EjemArreglo objArray = new EjemArreglo();
   Scanner entrada = new Scanner(System.in);
   int opc, cas;
   
   do{
      System.out.println("ARREGLO");
      System.out.println("1. Llenar Arreglo");
      System.out.println("2. Mostrar Arreglo");
      System.out.println("3. Buscar N�meros Pares");
      System.out.println("4. Salir");
      System.out.print("Elige una opci�n....  ");
      opc = entrada.nextInt();
      
      switch(opc)   
      {
        case 1: objArray.Llenar();
               break;
        case 2: objArray.Mostrar();
               break;
        case 3: do{
                 System.out.println("�Que casilla quieres verificar? [1-10]");
                 cas = entrada.nextInt();
               }while(cas > 10 | cas < 1); //que no sobrepase el array
               if(objArray.ParImpar(cas))
                    System.out.println("La casilla " +cas+ 
                     " con valor " + objArray.ObtenerValor() +
                      " es par");
                   else
                    System.out.println("La casilla " +cas+ 
                     " con valor " + objArray.ObtenerValor() +
                      " es impar");
               break;
        case 4: System.exit(0);
               break;
        default: System.out.println("Opci�n NO v�lida");
       }
    }while(opc!=4);
  }
}