import java.util.*;

   //asiento del 1 al 100

public class Auditorio{

   Scanner entrada = new Scanner(System.in);//entradas
   int[] asientos = new int[100], casillas = new int[100];; //100 asientos
   String[] asientos_ocupados = new String[100];
   int venta = 0, vendidos = 0, noVendidos = 100;
   String ocupante = new String();
   String msjNombre = "";
   
   public void iniciar(){
      mostrarMenu();
   }//iniciar
   
   public void mostrarMenu(){
      int opc = 0;
      do{
         System.out.println("* * *  M  E  N  U  * * * ");
         System.out.println(" 1). Vender boleto ");
         System.out.println(" 2). Consultar asiento ");
         System.out.println(" 3). Total vendidos ");
         System.out.println("[0]. Terminar ");
         opc = entrada.nextInt();
         switch(opc){
            case 1: venderBoleto(); 
            break;
            case 2: consultarAsiento();
            break;
            case 3: System.out.println("Vendido hasta ahora: $"+getTotalVendido()+".00 con: "+getBoletosVendidos()+" boletos vendidos");
            break;
            case 0: System.exit(0);//salir
            break;
            default: iniciar(); // aparece de nuevo el menu si se selecciono una opcion no valida
            break;
         }//switch
      }while(opc !=0 );
   }//menu
   
   public void venderBoleto(){
      int asiento = 0;
      do{
         System.out.println("Elija asiento: ");
         asiento =  entrada.nextInt();
      }while(asiento <= 0 | asiento > 100);
      if(consultarAsiento(asiento)){//SI ESTA DISPONIBLE
         System.out.println("A nombre de: ");
         String nombre = entrada.next();
         asientos_ocupados[asiento-1] = nombre;
         sumarVentas(asiento);
         
      }else{
         do{
            System.out.println("Elija otro asiento, comprado por: "+getOcupante());
            asiento = entrada.nextInt();
            consultarAsiento(asiento);
         }while(consultarAsiento(asiento) == false);   
      System.out.println("A nombre de: ");
      String nombre = entrada.next();
      asientos_ocupados[asiento-1] = nombre;
      sumarVentas(asiento);
      }//if-else
   }//venderBoleto
   
   public void consultarAsiento(){//sobrecarga de metodo
      System.out.println(" 1) Buscar por casilla"); 
      System.out.println(" 2) Buscar por nombre");
      System.out.println(" 3) Mostrar todos los disponibles");
      System.out.println("[0] Menu inicial");
      int opc = entrada.nextInt();
      switch(opc){
         case 0:mostrarMenu();
         case 1:
            System.out.println("Ingrese casilla a consultar"); 
            int c = entrada.nextInt();
            if(consultarAsiento(c)) System.out.println("Asiento disponible");
            else System.out.println("Asiento no disponible, comprado por: "+getOcupante());  
         break;
         
         case 2: System.out.println("Ingrese nombre a buscar:"); 
                  String name = entrada.next();
                  if(consultarAsiento(name)){ System.out.println("Coincidencia encontrada, encontrado en:"); mostrarAsientosNombre(); System.out.println(""); msjNombre = "";}//eliminar msjNombre si se altera algo
                  else System.out.println("Coincidencia no encontrada");  
         case 3: System.out.println("Asientos disponibles: "+getBoletosNoVendidos()+": "); mostrarAsientos(); System.out.println("");
         
         //default:  consultarAsiento();
         break;
      }//switch
   }//consultarAsiento
   
   public boolean consultarAsiento(int casilla){//true = disponible
      boolean estado = false;
      if(asientos_ocupados[casilla-1]==null) estado = true;
      else{ 
         estado = false;
         ocupante = asientos_ocupados[casilla-1];
      }
      return estado;
   }//consultarAsiento
   
   public boolean consultarAsiento(String nombre){//true = coincidencia, NO disponible
      boolean estado = false;
      int p = 0;//indice de asientos con nombre buscado
      for(int i = 0; i < asientos_ocupados.length; i++){// i es el asiento en el que se encuentra
         if(nombre.equalsIgnoreCase(asientos_ocupados[i])){
            estado = true;
            msjNombre += (i+1)+",";
            //System.out.print(msjNombre);
            //System.out.print("Ubicado en: "+(i+1));
            //casillas[p] = i;
            //p++; //incrementar indice
         }
      }//for
      //for(String persona : asientos_ocupados){
      //   if(nombre.equalsIgnoreCase(persona)) estado = true;
      //}//for
      return estado;
   }//consultarAsiento
   
   public void sumarVentas(int casilla){
      if(casilla < 20) totalVendido(300);
      else if (casilla > 19 && casilla < 50){ totalVendido(250);}
      else totalVendido(200);
      //if(casilla < 20)
   }//sumarVentas
   
   public void totalVendido(int valor){
      venta += valor;
      vendidos++;
      noVendidos--;
   }//totalVendido
   
   public void mostrarAsientosNombre(){
      for(int i = 0; i < casillas.length; i++){
         if(casillas[i] != 0) System.out.print(casillas[i+1]);
      }//for
      System.out.print(msjNombre);
   }//getAsientos
   
   public void mostrarAsientos(){
      for(int i = 0; i < asientos.length; i++){
         if(asientos_ocupados[i]==null) System.out.print((i+1)+", ");
      }//for
   }//mostrarAsientos
   
   public int getTotalVendido(){
      return venta;
   }//totalVendido
   
   public int getBoletosVendidos(){
      return vendidos;
   }//totalVendido
   
   public int getBoletosNoVendidos(){
      return noVendidos;
   }//getBoletosNoVendidos
   
   public String getOcupante(){
      return ocupante;
   }//totalVendido
   
}//class