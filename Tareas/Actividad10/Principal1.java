public class Principal1{

   public static void main(String[] args){
      Arreglo objArreglo = new Arreglo();
      objArreglo.iniciar();
      
      int[] array = objArreglo.a_ordenado;//cambiar por arreglo 
      int[] a_original = objArreglo.copia;//arreglo original
      
      System.out.println("*** Mostrar arreglo ordenado***");
      for (int i: array) {
        System.out.println (i);
      }
      System.out.println("*** Mostrar arreglo original***");
      for (int i: a_original) {
        System.out.println (i);
      }
   }//main
   
}//class