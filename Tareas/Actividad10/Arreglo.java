import java.util.*;

public class Arreglo{

   Scanner entrada = new Scanner(System.in); //entradas
   int[] a_ordenado = new int[20], arreglo = new int[20], copia = new int[20]; //arreglo de enteros
   int mayor = 0; // variables enteras
   
   public void iniciar(){
      llenarArreglo();
      ordenarArreglo();
   }
   
   public void llenarArreglo(){
      for(int i = 0; i < arreglo.length; i++){
         System.out.println("Valor en ["+(i+1)+"]");
         int n = entrada.nextInt();
         if(n <= 0){
            do{   
               System.out.println("Ingrese un valor positivo en ["+(i+1)+"]");
               n = entrada.nextInt();
            }while(n <= 0);
         }//if
         arreglo[i] = n;//asigno el valor a la casilla del arreglo correspondiente
         copia[i] = n;//mismo valor para copiar el arreglo
      }//for
   }//llenarArreglo
   
   public void ordenarArreglo(){
      mayor = 0; int cas = 0;
      for(int i = 0; i < a_ordenado.length; i++){
         for(int c = 0; c < arreglo.length; c++){
            if ( arreglo[c] > mayor ){
               mayor = arreglo[c]; 
               cas = c;
            }//if   
         }//for
         a_ordenado[i] = arreglo[cas];
         arreglo[cas] = 0;
         mayor = 0;
      }//for
   }//llenarArreglo
   
}//class