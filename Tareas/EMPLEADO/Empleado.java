import java.util.*;

public class Empleado {

   String nombreEmpleado;
   double salario;
   
   public void altaEmpleado(){
      Scanner entrada = new Scanner(System.in);
      System.out.println("Ingrese nombre del empleado");
      nombreEmpleado = entrada.nextLine();
      System.out.println("Ingrese numero de horas trabajadas");
      double hrs_Trabajadas = entrada.nextDouble();
      System.out.println("Ingrese pago por hora");
      double pago = entrada.nextDouble();
      System.out.println("Ingrese numero de faltas registradas");
      int faltas = entrada.nextInt();
      System.out.println("Ingrese descuento por falta que se registre");
      int descuento = entrada.nextInt();
      calcularSalario(pago, hrs_Trabajadas,faltas,descuento);
   }//altaEmpleado
   
   public void calcularSalario(double pago, double hrs, int faltas, double descuento){
      salario = (hrs * pago);
      salario -= salario * 0.10;
      for(int i = 1; i < faltas; i++){ 
         salario -= descuento;
      }
   }//calcularSalario
   
   public String getNombre(){
      return nombreEmpleado;
   }//getNombre
   
   public double getSalario(){
      return salario;
   }//getSalario
}//empleado