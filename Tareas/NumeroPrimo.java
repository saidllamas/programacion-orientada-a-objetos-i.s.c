import java.util.*;

/*
*  Jesus Said Llamas Manriquez I.S.C. 03/02/16
*  El siguiente codigo lee un numero y detecta si es o no un 
*  numero primo, tiene los metodos:
*  leerNumeros() y esPrimo()
*  TAREA
*/

public class NumeroPrimo{
   
   public static void main(String[] args){
      System.out.println(leerNumero());
   }//main
   
   public static String leerNumero(){
      Scanner entrada = new Scanner(System.in);
      
      System.out.println("Ingrese un numero.");
      int numero = entrada.nextInt();
      
      return esPrimo(numero);
   }//leerNumero
   
   public static String esPrimo(int numero){
      String msj = "";
      int divisibles = 0;
      
      for(int i = 1; i <= numero; i++){
         if((numero % i)==0) divisibles++;
      }//for
      
      if(divisibles > 2)msj = "No es primo";
      else if (divisibles > 0 && divisibles < 3)msj = "Es primo";
      
      return msj;
   }//esPrimo
   
}//clase