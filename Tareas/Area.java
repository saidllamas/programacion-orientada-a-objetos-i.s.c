import java.util.*;

/*
* Jesus Said Llamas Manriquez I.S.C. 29/01/16
*  PRACTICA-TAREA
*/

public class Area{

   //variables
   private static double area;
   private static int opc;
   private static final double PI = 3.141516;
   private static Scanner entrada = new Scanner(System.in);
   
   //Metodos
   public static void main(String []args){
      menu();
      //mostrarArea(); si hago esto al mandarme el defalut del menu() se pierde la secuencia a esta instruccion
   }//main
   
   public static void menu(){
      System.out.println(" M  E  N  U");
      System.out.println("1.-Area Triangulo \n2.-Area Cuadrado \n3.-Area Rectangulo \n4.-Area Circulo");
      opc = entrada.nextInt();
      switch(opc){
         case 1: areaTriangulo(); 
                  mostrarArea();
         break;
         case 2: areaCuadrado();
                  mostrarArea();
         break;
         case 3: areaRectangulo();
                  mostrarArea();
         break;
         case 4: areaCirculo();
                  mostrarArea();
         break;
         default: menu();
      }//switch
   }//menu
   public static void areaTriangulo(){
      int base, altura;
      System.out.println("Ingresa base");
      base = entrada.nextInt();
      System.out.println("Ingresa altura");
      altura = entrada.nextInt();
      area = ( base * altura ) / 2;
   }//areaTriagulo
   
   public static void areaCuadrado(){
      int lado;
      System.out.println("Ingresa lado");
      lado = entrada.nextInt();
      area = lado * lado;
   }//areaCuadrado
   
   public static void areaRectangulo(){
      int base, altura;
      System.out.println("Ingresa base");
      base = entrada.nextInt();   
      System.out.println("Ingresa atura");
      altura = entrada.nextInt();
      area = base * altura;
   }//areaRectangulo
   
   public static void areaCirculo(){
      int radio;
      System.out.println("Ingresa radio");
      radio = entrada.nextInt();
      area = PI * (radio * radio);
   }//areaCirculo
   
   public static void mostrarArea(){
      System.out.println("El area es: "+area);
      System.out.println("¿Nuevo calculo? 1.-Si Otro numero.-No");
      opc = entrada.nextInt();
      if(opc == 1) menu(); else{ System.out.println("Calculos terminados"); }
   }//mostrarArea
   
}//class