import java.util.*;

/*
*  Jesus Said Llamas Manriquez I.S.C. 29/01/16
*  Dadas el valor de las monedas: Dolar = 18.2468; Euro = 19.61; Libra = 25.7995; Yen = 6.65
*  convertir de pesos mexicanos a esas monedas, la clase tiene el metodo menu(), conversionDOLARES(),
*  conversionLibras(), conversionYenes(), conversionEuros() 
*  TAREA
*/

public class Conversiones{

   //variables
   private static int pesos, opc;//Los pesos a convertir y opc del menu elegida
   private static double conversion; //valor de la conversion a la nueva moneda
   private static Scanner entrada = new Scanner(System.in); 
   
   //Metodos
   public static void main(String []args){
      menu();
   }//main
   
   public static void menu(){
      System.out.println(" M  E  N  U... Conertidor de monedas");
      System.out.println("1.- Pesos mexicanos a dolares \n2.- Pesos mexicanos a libras esterlinas \n3.- Pesos mexicanos a Yenes \n4.- Pesos mexicanos a euros \n5. Terminar");
      opc = entrada.nextInt();
      if(opc < 1 | opc > 5){ System.out.println("¡Error! regresando al menu"); menu(); }
      else if(opc == 5){ System.out.println("Conversiones terminadas"); System.exit(0);}
      System.out.println("Bien, ahora ingresa los pesos mexicanos");
      pesos = entrada.nextInt();
      switch(opc){
         case 1: conversionDolares();
         break;
         case 2: conversionLibras();
         break;
         case 3: conversionYenes();
         break;
         case 4: conversionEuros();
         break;
      }//switch
      mostrarConversion();
   }//menu
   
   public static void conversionDolares(){
      final double DOLAR = 18.2468;
      conversion = pesos / DOLAR;
   }//conversionDolares
   
   public static void conversionLibras(){
      final double LIBRA = 25.7995;
      conversion = pesos / LIBRA;
   }//conversionDolares
   
   public static void conversionYenes(){
      final double YEN = 6.65;
      conversion = pesos / YEN;
   }//conversionDolares
   
   public static void conversionEuros(){
      final double EURO = 19.61;
      conversion = pesos / EURO;
   }//conversionDolares
   
   public static void mostrarConversion(){
      System.out.println("Conversion: "+conversion);
      System.out.println("¿Desea realizar una nueva conversion? 1.-Si Otro numero.-Terminar");
      opc = entrada.nextInt();
      if(opc == 1) menu(); { System.out.println("Conversiones terminadas"); System.exit(0);}
   }//mostrarConversion
   
}//class   