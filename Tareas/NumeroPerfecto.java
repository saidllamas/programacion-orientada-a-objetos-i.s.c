import java.util.*;

/*
*  TAREA 15-02-16
*/

public class NumeroPerfecto{

   public static void main(String[] args){
   
   int numero = 0;
   Scanner entrada = new Scanner(System.in);   
   System.out.println("Ingrese numero entero");
   do{      
      numero = entrada.nextInt();
      System.out.println(esPerfecto(numero));//impresion de si es o no perfecto
      System.out.println("Ingrese nuevo numero ó [0]Terminar");
   }while(numero > 0);
   
   }//main
   
   public static String esPerfecto(int numero){
   
      int num = 0, suma = 0;
      String msj = "";
      
      for(int i = 1; i <= numero; i++){
         num = i;
         if(numero % num == 0 && numero != num) suma += num;
      }//for
      if(suma == numero) msj = "Es perfecto";
      else msj ="No es perfecto";
      
      return msj;
   }//esPerfecto
   
}//clase