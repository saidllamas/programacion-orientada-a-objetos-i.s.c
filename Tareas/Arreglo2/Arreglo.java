import java.util.*;

public class Arreglo{

   static Scanner entrada = new Scanner(System.in);
   static double[] array;// = new double[tamaño];
   static int tamaño = 0;
   
   public void menu(){
      int opc = 0;
      do{
         System.out.println("***    M  E  N  U     ***");
         System.out.println("1. Elegir tamaño arreglo \n2. Llenar arreglo \n3. Sumar y mostrar suma");
         opc = entrada.nextInt();
         switch(opc){
            case 1: elegirTamaño();
            break;
            case 2: llenarArreglo(array);
            break;
            case 3: System.out.println(suma(array));
            break;
         }//switch
      }while(opc != 0);   
   }//menu
   
   public void elegirTamaño(){
      System.out.println("Ingrese el tamaño");
      tamaño = entrada.nextInt();
      array = new double[tamaño];
   }
   
   public void llenarArreglo(double [] arreglo){
      for(int i = 0; i < arreglo.length; i++){
         System.out.println("Casilla["+i+"]");
         arreglo[i] = entrada.nextDouble();
      }//for
   }
   
   public double suma(double [] arreglo){
      double suma = 0;
      for(int i = 0; i < arreglo.length; i++){
         suma += arreglo[i];
      }//for
      return suma;
   }//suma
   
}//class