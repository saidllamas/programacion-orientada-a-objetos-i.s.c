import java.util.*;

/*
*  Jesus Said Llamas Manriquez I.S.C. 03/02/16
*  El siguiente codigo lee tres numeros enteros y devuelve el mayor
*  de estos, tiene los metodos:
*  leerNUmeros() y esMayor()
*  TAREA
*/

public class NumeroMayor{

   public static void main(String[] args){
      System.out.println("El numero mas grande es: "+leerNumeros());
      //¿cual es la explicacion de no imprimir y luego retornar el valor?
      //idea: primero imprime, despues retorna el valor, pero no...
      //...se va directo al metodo leerNumeros y despues imprime
   }//main
   
   public static int leerNumeros(){
      Scanner entrada = new Scanner(System.in);
      
      System.out.println("Ingresa el primer numero.");
      int n1 = entrada.nextInt();
      System.out.println("Ingresa el segundo numero.");
      int n2 = entrada.nextInt();
      System.out.println("Ingresa el tercer numero.");
      int n3 = entrada.nextInt();
      
      return esMayor(n1,n2,n3);
   }//leerNumeros
   
   public static int esMayor(int n1, int n2, int n3){
      int maximo = Math.max(n1,n2);
      
      if(maximo<n3)maximo = n3;
      
      return maximo;
   }//esMayor
   
   
}//clase