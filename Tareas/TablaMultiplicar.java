import java.util.*;

/* Jesús Said Llamas Manriquez
*  Elaborar un programa que calcule la tabla de multiplicar de un numero
*  entero N, solicitar N y el rango que se desea obtener, realizar el calculo
*  de la multiplicacion, y las impresiones en diferentes metodos.   
*  TAREA 15-02-16
*/

public class TablaMultiplicar{

   public static void main(String[] args){
      ingresarNumero();
   }//main
   
   public static void ingresarNumero(){
      Scanner entrada = new Scanner(System.in);
      int opc;
      do{
         System.out.println("Ingresa el numero que deseas descubrir la tabla de multiplicar");
         int numero = entrada.nextInt();
         System.out.println("Ingresa el rango de la multiplicacion");
         int rango = entrada.nextInt();
         calcular(numero, rango);
         System.out.println("¿Deseas agregar nuevamente numeros? [0]No");
         opc = entrada.nextInt();
      }while(opc > 0);   
      
   }//ingresar numero
   
   public static void calcular(int numero, int rango){
      for(int i = 1; i <= rango; i++){
         int res = numero * i;
         System.out.print(numero+ " * " +i +" = ");
         imprimir(res);
      }
   }//calcular
   
   public static void imprimir(int res){
      System.out.println(res);
   }//imprimir
}//clase