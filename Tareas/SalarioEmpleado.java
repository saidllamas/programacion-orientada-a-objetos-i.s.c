import java.util.*;

/*
*  14-02-16
*  TAREA
*/

public class SalarioEmpleado{

   static Scanner entrada = new Scanner(System.in);

   public static void main(String[] args){
      Scanner entrada = new Scanner(System.in);
      int opc;
      do{
         double salario = altaEmpleado();
         System.out.println("Su salario es: "+salario);
         
         System.out.println("¿Desea calcular nuevo empleado? [0] Terminar");
         opc = entrada.nextInt();
      }while(opc > 0);
   }//main
   
   public static double altaEmpleado(){
      System.out.println("Ingrese No. de empleado: ");
      int numero = entrada.nextInt();
      System.out.println("Ingrese nombre: ");
      String nombre = entrada.next();
      System.out.println("Ingrese direccion: ");
      String direccion = entrada.next();
      System.out.println("Ingrese horas trabajadas: ");
      float hrs_Trabajadas = entrada.nextFloat();
      System.out.println("Ingrese salario x hora: ");
      float sal_Hora = entrada.nextFloat();
      return calcularSalario(sal_Hora, hrs_Trabajadas);
   }//altaEmpleado
   
   public static double calcularSalario(float sal_Hora, float hrs_Trabajadas){
      double salario = sal_Hora * hrs_Trabajadas;
      return salario;
   }//calcularSalario
   
}//clase