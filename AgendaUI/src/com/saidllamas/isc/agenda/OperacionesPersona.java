package com.saidllamas.isc.agenda;

import java.util.Scanner;

import javax.swing.JOptionPane;

public class OperacionesPersona {
	
	static Persona[] personas = new Persona[100]; //Personas agregadas 1-100- aqui se almacenan todas las personas
	Persona objPersona = new Persona(); //Persona agregada
	private int _id = 0; //identificador unico para cada persona, inicia en 0
	private Object[] object = new Object[7];
	
	private boolean restar = false;
	Scanner entrada; //entradas por teclado
	
	//si el nombre est vacio entoces incrementar en 1 el limite del ciclo
	
	public OperacionesPersona(){
	}//constructor vacio
	
	public int consularID(String name){
		int id = 0;
		for(int i = 0; i < 100; i++){
			if(personas[i].getNombre().equals(name)){
				id = i;
				//id = personas[i].getId();
				System.out.println("Encontrado en el arreglo con la posicion: "+id);
				break;
			}//if
		}//for
		return id;
	}//consultarID
	
	public void agregarContacto(String name, String month, String day, String address, String email, String telF, String telM, int id){
		personas[id] = new Persona(name, month, day, address, email, telF, telM, id);//_id = fila
		/*
		personas[id].setNombre(name);
		personas[id].setMesNacimiento(month);
		personas[id].setDiaNacimiento(day);
		personas[id].setDireccion(address);
		personas[id].setCorreoElectronico(email);
		personas[id].setTelefonoFijo(telF);
		personas[id].setTelefonoMovil(telM);
		personas[id].setID(id);
		*/
		System.out.println(personas[id].getNombre()+" agregado(a) correctamente, con el ID:"+id);
		setObjet(id); //creo el objeto para agregar fila a la tabla
		_id = id;
		for(int i = 0; i <= id; i++){
			if(personas[i].getNombre().equals(".")) System.out.println("");
			else System.out.println("indice: "+i+", "+personas[i].getNombre()+", "+personas[i].getId());
		}
	}//agregarContacto	
	
	public static int consultarDia(int id){
		int d = Integer.parseInt(personas[id].getDiaNacimiento());
		return d;
	}//consultarDia
	
	public int reducirIDs(int desde, int hasta){
		int id = 0; //cada que encuentre un nombre real != . entonces incrementar en 1
		//desde+=2;
		//restar = false;
		//desde++;//para que inicie uno despues de ese id
		desde = 0;
		System.out.println(" --- desde: "+desde+" hasta: "+hasta+" reducir en 1 el ID ---");
		//int indice = 0;
		for(; desde < hasta; desde++){
			if(personas[desde].getNombre().equals(".")){
				//posicion vacia, hay que saltarsela para que no reduzca aqui
				System.out.println("Vacio");
				hasta++;
				restar = true;
			}else{
				personas[desde].setID(id);
				System.out.println(personas[desde].getNombre()+" nuevo ID: "+personas[desde].getId());
				id++;
			}//else
			
		}//for
		System.out.println("termina reduccion de ID, ultimo ID generado: "+id);
		_id = id;
		return id;
	}//reducirIDs
	
	public int consultarContacto(String name){
		int fila = -1;
		System.out.println("*****Consulta general****");
		for (int i = 0; i <= 99; i++){//cambiar por <=99 si hay error en consultas
			System.out.println(personas[i].getNombre()+", "+personas[i].getId());
			if(personas[i].getNombre().equalsIgnoreCase(name)){
				if (restar){
					System.out.println("Hubo eliminacion antes de consultar");
					fila = personas[i].getId();//posicion donde se encontro coincidencia
					JOptionPane.showMessageDialog(null, "Se encontro coincidencia en la fila: "+(fila+1));//para que no muestre 0
					break;
				}
				else{
					System.out.println("No hubo eliminacion antes de consultar");
					fila = personas[i].getId();//posicion donde se encontro coincidencia
					System.out.println("Se encontro en "+fila);
					JOptionPane.showMessageDialog(null, "Se encontro coincidencia en la fila: "+(fila+1));//para que no muestre 0
					break;
				}//else--
			}//if
			if(i==98) break;
		}//for
		System.out.println("*****TERMINA Consulta general****");
		return fila;
	}//borrarContacto
	
	public static int consultarMes(int id){
		if(personas[id].getMesNacimiento().equals("Enero")) return 1;
		else if(personas[id].getMesNacimiento().equals("Febrero")) return 2;
		else if(personas[id].getMesNacimiento().equals("Marzo")) return 3;
		else if(personas[id].getMesNacimiento().equals("Abril")) return 4;
		else if(personas[id].getMesNacimiento().equals("Mayo")) return 5;
		else if(personas[id].getMesNacimiento().equals("Junio")) return 6;
		else if(personas[id].getMesNacimiento().equals("Julio")) return 7;
		else if(personas[id].getMesNacimiento().equals("Agosto")) return 8;
		else if(personas[id].getMesNacimiento().equals("Septiembre")) return 9;
		else if(personas[id].getMesNacimiento().equals("Octubre")) return 10;
		else if(personas[id].getMesNacimiento().equals("Noviembre")) return 11;
		else if(personas[id].getMesNacimiento().equals("Diciembre")) return 12;
		else return 0;
	}//consultarMes
	
	public void setObjet(int id){
		object[0] = personas[id].getNombre();
		object[1] = personas[id].getDiaNacimiento() + " - " + personas[id].getMesNacimiento(); 
		object[2] = personas[id].getDireccion();
		object[3] = personas[id].getCorreoElectronico();
		object[4] = personas[id].getTelefonoFijo();
		object[5] = personas[id].getTelefonoMovil();
		object[6] = personas[id].getId();
	}//getObject
	
	public Object[] getObjet(){
		return object;
	}//getObject
	
	public void eliminarContacto(int id, int fila, String msj){
		System.out.println(personas[id].getNombre()+" "+msj);
		JOptionPane.showMessageDialog(null, personas[id].getNombre()+" "+msj);
		personas[id] = new Persona(".", "", "", "", "", "", "", 0);//cambiar por variable id
//		pasarDatos(id, fila);
	}//eliminarContacto
	
	/*public void pasarDatos(int lugarAOcupar, int hasta){
		int i = lugarAOcupar;
		for(; i < hasta; lugarAOcupar++){
			personas[i] = new Persona(personas[i+1].getNombre(),
			personas[i+1].getMesNacimiento(), personas[i+1].getDiaNacimiento(),
			personas[i+1].getDireccion(), personas[i+1].getCorreoElectronico(),
			personas[i+1].getTelefonoFijo(), personas[i+1].getTelefonoMovil(), i);
			//Persona(String name, String month, String day, String address, String email, String telF, String telM, int id) {
		}//for
	}//pasarDatos*/

}//class