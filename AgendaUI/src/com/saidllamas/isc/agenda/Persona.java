package com.saidllamas.isc.agenda;

public class Persona {
	
	private String nombre = "", mes = "", dia = "", direccion = "", correoElectronico = "", telefonoFijo = "", telefonoMovil = "";
	private int _id = 0;
	
				/*	METODOS SET	*/
	public Persona(){
		
	}//constructor default
	
	public Persona(String name, String month, String day, String address, String email, String telF, String telM, int id) {
		nombre = name;
		mes = month;
		dia = day;
		direccion = address;
		correoElectronico = email;
		telefonoFijo = telF;
		telefonoMovil = telM;
		_id = id;
	}//constructor
	
	public void setNombre(String name){
		nombre = name;
	}//setNombre
	
	public void setMesNacimiento(String month){
		mes = month;
	}//setMes
	
	public void setDiaNacimiento(String day){
		dia = day;
	}//setDia
	
	public void setDireccion(String address){
		direccion = address;
	}//setDireccion
	
	public void setCorreoElectronico(String email){
		correoElectronico = email;
	}//setCorreoElectronico
	
	public void setTelefonoFijo(String telF){
		telefonoFijo = telF;
	}//setTelefonoFijo
	
	public void setTelefonoMovil(String telM){
		telefonoMovil = telM;
	}//setTelefonoMovil
	
	public void setID(int id) {
		_id = id;
	}//setID
				/*	METODOS GET	*/
	
	public String getNombre(){
		return nombre;
	}//getNombre
	
	public String getMesNacimiento(){
		return mes;
	}//getFechaNacimiento
	
	public String getDiaNacimiento(){
		return dia;
	}//getFechaNacimiento
	
	public String getDireccion(){
		return direccion;
	}//getDireccion
	
	public String getCorreoElectronico(){
		return correoElectronico;
	}//getCorreoElectronico
	
	public String getTelefonoFijo(){
		return telefonoFijo;
	}//getTelefonoFijo
	
	public String getTelefonoMovil(){
		return telefonoMovil;
	}//getTelefonoMovil
	
	public int getId(){
		return _id;
	}//getId
	
}//class