package com.saidllamas.isc.agenda;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import java.awt.Choice;
import java.awt.Label;
import java.awt.Font;
import javax.swing.table.DefaultTableModel;

import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.SwingConstants;

public class VentanaInicial {

	private JFrame frmAgendaUnidad;
	private JTextField txtNombre;
	private JTextField txtDireccion;
	private JTextField txtTelF;
	private JTextField txtTelM;
	private JTable table;
	private Choice mes, dia;
	private JTextField txtMail;
	private JTextField txtBusqueda;
	private DefaultTableModel modelo = new DefaultTableModel();
	
	private int seleccion; //que fila es seleccionada por el usuario en la tabla
	private int fila = 0; //cuantas filas hay en la tabla
	private boolean editar; //control para saber si hay que guardar los cambios o agregar a una persona
	private int posReal = 0;
	
	private OperacionesPersona objOperaciones = new OperacionesPersona();
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaInicial window = new VentanaInicial();
					window.frmAgendaUnidad.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaInicial() {
		initialize();
	}

	private void initialize() {	

		frmAgendaUnidad = new JFrame();
		frmAgendaUnidad.setResizable(false);
		frmAgendaUnidad.setTitle("J. Said Llamas Manriquez I.S.C.");
		frmAgendaUnidad.setBounds(100, 100, 493, 557);
		frmAgendaUnidad.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgendaUnidad.getContentPane().setLayout(null);
		
		JLabel lblAgenda = new JLabel("Agenda");
		lblAgenda.setForeground(Color.DARK_GRAY);
		lblAgenda.setFont(new Font("Tahoma", Font.PLAIN, 46));
		lblAgenda.setBounds(149, 11, 196, 58);
		frmAgendaUnidad.getContentPane().add(lblAgenda);
		
		JLabel lblPersonas = new JLabel("");
		lblPersonas.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPersonas.setForeground(Color.GRAY);
		lblPersonas.setBounds(322, 209, 155, 14);
		frmAgendaUnidad.getContentPane().add(lblPersonas);
		
		JButton btnAgregarContacto = new JButton("Agregar ");
		btnAgregarContacto.setBackground(Color.WHITE);
		btnAgregarContacto.setForeground(Color.DARK_GRAY);
		btnAgregarContacto.setIcon(new ImageIcon("C:\\Almacenamiento\\programacion-orientada-a-objetos-i.s.c\\AgendaUI\\images\\add_user.png"));
		btnAgregarContacto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if( txtNombre.getText().equals("") && editar == false ) JOptionPane.showMessageDialog(null, "Ingrese un nombre");
				else if( dia.getSelectedIndex()==0 && editar == false ) JOptionPane.showMessageDialog(null, "Ingrese el dia de nacimiento");
				else if( mes.getSelectedIndex()==0 && editar == false ) JOptionPane.showMessageDialog(null, "Ingrese el mes de nacimiento");
				else{
					if(editar){

						//agregar
						if(posReal>fila){ fila = posReal;}

						//recojo los valores de los campos de texto
						String name = txtNombre.getText();
						String month = mes.getSelectedItem();
						String day = dia.getSelectedItem();
						String address = txtDireccion.getText();
						String email = txtMail.getText();
						String telF = txtTelF.getText();
						String telM = txtTelM.getText();

						modelo.removeRow(seleccion);
						//agrego a la persona
						objOperaciones.agregarContacto(name, month, day, address, email, telF, telM, fila);
						btnAgregarContacto.setText("Agregar");
						//regreso los campos de texto a editables
						txtNombre.setEditable(true);
						dia.setEnabled(true);
						mes.setEnabled(true);
						//actualizo la tabla para mostrar a la nueva persona
						actualizar();
						editar = false;
						//indicador de personas mostrandose en la tabla
						lblPersonas.setText("Personas agregadas: "+fila);
						objOperaciones.reducirIDs(0, fila);//eliminar si al editar-guardar hay problemas con los id
						//hasta aqui codigo anterior
						//termino la edicion ahora regresa al estado normal de agregar
						editar = false;
					}//editar
					else{
						if(posReal>fila){ fila = posReal;}
						//recojo los valores de los campos de texto
						String name = txtNombre.getText();
						String month = mes.getSelectedItem();
						String day = dia.getSelectedItem();
						String address = txtDireccion.getText();
						String email = txtMail.getText();
						String telF = txtTelF.getText();
						String telM = txtTelM.getText();
						//agrego a la persona
						objOperaciones.agregarContacto(name, month, day, address, email, telF, telM, fila);
						//actualizo la tabla para mostrar a la nueva persona
						actualizar();
						objOperaciones.reducirIDs(0, fila);
						//indicador de personas mostrandose en la tabla
						lblPersonas.setText("Personas agregadas: "+fila);
					}//agregar
					//listo para agregar al usuario
				}//else
			}//actionPerformed
		});
		btnAgregarContacto.setBounds(322, 139, 157, 58);
		frmAgendaUnidad.getContentPane().add(btnAgregarContacto);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(88, 80, 140, 20);
		frmAgendaUnidad.getContentPane().add(txtNombre);
		txtNombre.setColumns(10);
		
		txtDireccion = new JTextField();
		txtDireccion.setBounds(322, 80, 157, 20);
		frmAgendaUnidad.getContentPane().add(txtDireccion);
		txtDireccion.setColumns(10);
		
		dia = new Choice();
		dia.setBounds(40, 142, 46, 20);
		dia.add("");
		for(int i = 1; i < 31; i++){
			dia.addItem(""+i); //llenar de dias
		}
		frmAgendaUnidad.getContentPane().add(dia);
		
		Label label = new Label("Dia:");
		label.setForeground(Color.DARK_GRAY);
		label.setBounds(10, 142, 33, 22);
		frmAgendaUnidad.getContentPane().add(label);
		
		Label label_1 = new Label("Mes:");
		label_1.setForeground(Color.DARK_GRAY);
		label_1.setBounds(121, 140, 46, 22);
		frmAgendaUnidad.getContentPane().add(label_1);
		
		mes = new Choice();
		mes.setBounds(173, 142, 108, 20); 
		mes.add("");//inciia vacio
		mes.add("Enero"); mes.add("Febrero"); mes.add("Marzo"); mes.add("Abril"); mes.add("Mayo"); mes.add("Junio");
		mes.add("Julio"); mes.add("Agosto"); mes.add("Septiembre"); mes.add("Octubre"); mes.add("Noviembre"); mes.add("Diciembre");   
		frmAgendaUnidad.getContentPane().add(mes);
		
		Label label_2 = new Label("Tel. Fijo:");
		label_2.setForeground(Color.DARK_GRAY);
		label_2.setBounds(10, 111, 46, 22);
		frmAgendaUnidad.getContentPane().add(label_2);
		
		txtTelF = new JTextField();
		txtTelF.setBounds(88, 111, 140, 20);
		frmAgendaUnidad.getContentPane().add(txtTelF);
		txtTelF.setColumns(10);
		
		txtTelM = new JTextField();
		txtTelM.setColumns(10);
		txtTelM.setBounds(322, 111, 157, 20);
		frmAgendaUnidad.getContentPane().add(txtTelM);
		
		Label label_3 = new Label("Tel. Movil:");
		label_3.setForeground(Color.DARK_GRAY);
		label_3.setBounds(253, 111, 54, 22);
		frmAgendaUnidad.getContentPane().add(label_3);
		
		JLabel lblNewLabel = new JLabel("Contactos...");
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setFont(new Font("Tahoma", Font.ITALIC, 14));
		lblNewLabel.setBounds(188, 203, 92, 23);
		frmAgendaUnidad.getContentPane().add(lblNewLabel);
		
		//table = new JTable(modelo);
		table = new JTable(modelo);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION); //seleccion unicamente de una fila
		table.setFocusable(false);
		//modelo para la tabla
		modelo.addColumn("Nombre");
		modelo.addColumn("Fecha de nacimiento");
		modelo.addColumn("Direccion");
		modelo.addColumn("Correo electronico");
		modelo.addColumn("Telefono Fijo");
		modelo.addColumn("Telefono movil");
		//termina el modelo para la tabla
		table.setBounds(10, 233, 469, 245);
		frmAgendaUnidad.getContentPane().add(table);
		
		txtMail = new JTextField();
		txtMail.setBounds(121, 177, 160, 20);
		frmAgendaUnidad.getContentPane().add(txtMail);
		txtMail.setColumns(10);
		
		Label label_4 = new Label("Nombre:");
		label_4.setForeground(Color.DARK_GRAY);
		label_4.setBounds(10, 78, 46, 22);
		frmAgendaUnidad.getContentPane().add(label_4);
		
		Label label_5 = new Label("Correo electronico:");
		label_5.setForeground(Color.DARK_GRAY);
		label_5.setBounds(10, 175, 105, 22);
		frmAgendaUnidad.getContentPane().add(label_5);
		
		Label label_6 = new Label("Direccion:");
		label_6.setForeground(Color.DARK_GRAY);
		label_6.setBounds(253, 78, 63, 22);
		frmAgendaUnidad.getContentPane().add(label_6);
		
		JButton btnBuscar = new JButton("");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(txtBusqueda.getText().equals("")) JOptionPane.showMessageDialog(null, "Ingrese el nombre a buscar");
				else {
					String p = txtBusqueda.getText();
					int f  = objOperaciones.consultarContacto(p);
					if(f<0) JOptionPane.showMessageDialog(null, "No se encontraron coincidencias");
					else table.addRowSelectionInterval(f, f);
					//buscar
				}
			}//actionPerformed
		});
		btnBuscar.setBackground(Color.WHITE);
		btnBuscar.setIcon(new ImageIcon("C:\\Almacenamiento\\programacion-orientada-a-objetos-i.s.c\\AgendaUI\\images\\search-loop-512.png"));
		btnBuscar.setBounds(446, 11, 33, 23);
		frmAgendaUnidad.getContentPane().add(btnBuscar);
		
		txtBusqueda = new JTextField();
		txtBusqueda.setToolTipText("Nombre a buscar");
		txtBusqueda.setBounds(322, 11, 108, 23);
		frmAgendaUnidad.getContentPane().add(txtBusqueda);
		txtBusqueda.setColumns(10);
		
		JButton btnEditarContacto = new JButton("Editar contacto");
		btnEditarContacto.setIcon(new ImageIcon("C:\\Almacenamiento\\programacion-orientada-a-objetos-i.s.c\\AgendaUI\\images\\edit-icon.png"));
		btnEditarContacto.setBackground(Color.WHITE);
		btnEditarContacto.setForeground(Color.DARK_GRAY);
		btnEditarContacto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				seleccion = table.getSelectedRow();
				if(seleccion < 0) JOptionPane.showMessageDialog(null, "Primero seleccione una fila");
				else { 
					//descativo los campos de texto para que no se puedan editar
					txtNombre.setEditable(false);
					dia.setEnabled(false);
					mes.setEnabled(false);
					//recojo los valores
					String nombre = String.valueOf(table.getValueAt(seleccion, 0)); //nombre
					String direccion = String.valueOf(table.getValueAt(seleccion, 2)); //Direccion
					String mail = String.valueOf(table.getValueAt(seleccion, 3)); //Email
					String telF = String.valueOf(table.getValueAt(seleccion, 4)); //Tel Fijo
					String telM = String.valueOf(table.getValueAt(seleccion, 5)); //Tel Movil
					//verifico el identificador a manipular
					int d = objOperaciones.consularID(nombre);
					dia.select(OperacionesPersona.consultarDia(d));
					mes.select(OperacionesPersona.consultarMes(d));
					objOperaciones.eliminarContacto(d, fila, "listo para editar");
					//asigno los valores
					txtNombre.setText(nombre);
					txtDireccion.setText(direccion);
					txtMail.setText(mail);
					txtTelF.setText(telF);
					txtTelM.setText(telM);
					btnAgregarContacto.setText("Guardar"); //boton agregar se convierte en guardar
					//empieza el proceso de editar
					editar = true; //mando a control que deben sobreescribirse los datos
				}//else
			}//actionPerformed
		});
		btnEditarContacto.setBounds(20, 499, 147, 23);
		frmAgendaUnidad.getContentPane().add(btnEditarContacto);
		
		JButton btnEliminarContacto = new JButton("Eliminar contacto");
		btnEliminarContacto.setBackground(Color.WHITE);
		btnEliminarContacto.setForeground(Color.DARK_GRAY);
		btnEliminarContacto.setIcon(new ImageIcon("C:\\Almacenamiento\\programacion-orientada-a-objetos-i.s.c\\AgendaUI\\images\\sign-delete-icon.png"));
		btnEliminarContacto.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				seleccion = table.getSelectedRow();
				if(seleccion < 0) JOptionPane.showMessageDialog(null, "Primero seleccione una fila");
				else{
					String n = String.valueOf(table.getValueAt(seleccion, 0)); //nombre
					int d = objOperaciones.consularID(n);
					objOperaciones.eliminarContacto(d, fila, "eliminado correctamente");
					modelo.removeRow(seleccion);
					if(posReal<fila){
						posReal = fila;	
					}
					fila--;
					lblPersonas.setText("Personas agregadas: "+fila);
					//fila = objOperaciones.reducirIDs(seleccion);
					//fila = objOperaciones.reducirIDs(d, fila); //si hago esto entonces se empiezan a sobreescribir datos y esta mal
					objOperaciones.reducirIDs(d, fila);
					//borrar
				}//else
			}//actionPerfomed
		});
		btnEliminarContacto.setBounds(311, 499, 151, 23);
		frmAgendaUnidad.getContentPane().add(btnEliminarContacto);
		
		JLabel label_7 = new JLabel("   ");
		label_7.setIcon(new ImageIcon("C:\\Almacenamiento\\programacion-orientada-a-objetos-i.s.c\\AgendaUI\\images\\cdguzman_logo.jpg"));
		label_7.setBounds(10, 11, 62, 58);
		frmAgendaUnidad.getContentPane().add(label_7);
		
		JLabel lblV = new JLabel("v. 1");
		lblV.setForeground(Color.LIGHT_GRAY);
		lblV.setBounds(230, 503, 33, 14);
		frmAgendaUnidad.getContentPane().add(lblV);
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(10, 233, 469, 255);
		scrollPane.setVisible(true);
		frmAgendaUnidad.getContentPane().add(scrollPane);
		
	}//class
	
	
	public void actualizar(){
		modelo.addRow(objOperaciones.getObjet()); 
		fila++;
		limpiarCampos();
	}//actualizar
	
	public void limpiarCampos(){
		txtNombre.setText("");
		txtDireccion.setText("");
		txtMail.setText("");
		txtTelF.setText("");
		txtTelM.setText("");
		dia.select(0);
		mes.select(0);
	}//limpiarDatos
}
